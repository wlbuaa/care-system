package com.buaa.concernandlove.dao;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class WarningStuDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    public int insert(int studentId){
        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from stu_warning where studentId=?",studentId);
        if(!list.isEmpty()) {
            return -1;
        }

        String sql="insert into stu_warning (studentId,warningCondition1,warningCondition2,warningCondition3,warningConditionEx1,warningConditionEx2,warningConditionEx3) values (?,?,?,?,?,?,?);";
        JSONObject warningCondition=new JSONObject();
        warningCondition.put("reporter",new JSONArray());
        warningCondition.put("question",new JSONArray());
        warningCondition.put("insideRelation",0);
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement=con.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1,studentId);
                preparedStatement.setString(2,warningCondition.toJSONString());
                preparedStatement.setString(3,warningCondition.toJSONString());
                preparedStatement.setString(4,warningCondition.toJSONString());
                preparedStatement.setString(5,warningCondition.toJSONString());
                preparedStatement.setString(6,warningCondition.toJSONString());
                preparedStatement.setString(7,warningCondition.toJSONString());
                return preparedStatement;
            }
        });
        return 1;
    }

    public int delete(int id){
        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from stu_warning where studentId=?",id);
        if(list.isEmpty()) {
            return -1;
        }
        String sql="delete from stu_warning where studentId = ?";
        return jdbcTemplate.update(sql, id);
    }

    public int changeStatus(int id){
        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from stu_warning where studentId=?",id);
        if(list.isEmpty()) {
            return -1;
        }
        boolean baned =  (boolean)list.get(0).get("is_ban");
        String sql = "update stu_warning set is_ban = ? where studentId = ?";
        return jdbcTemplate.update(sql, baned?false:true, id);
    }

    public Page<Map<String, Object>> getWarningStuList(Pageable pageable){
        List<Map<String, Object>> list = jdbcTemplate.queryForList("select s1.is_ban as disable, s2.stuId as id, s2.studentName as name, s2.phoneNum as phone from stu_warning s1, watch_student s2 where s1.studentId = s2.studentId limit " +
                pageable.getPageSize() + " offset " + pageable.getOffset());
        return new PageImpl<Map<String, Object>>(list, pageable, count_stu_warning());
    }

    public Page<Map<String, Object>> getWarningStuLog(int id, Pageable pageable){
        List<Map<String, Object>> list = jdbcTemplate.queryForList("select warningId, warningTime as datetime, level, solved from warning_record where studentId = ? order by warningTime desc limit " +
                pageable.getPageSize() + " offset " + pageable.getOffset(), id);
        return new PageImpl<Map<String, Object>>(list, pageable, count_warning_record(id));
    }

    public int count_warning_record (int id){
        return jdbcTemplate.queryForObject("select count(*) from warning_record where studentId = "+id+";", Integer.class);
    }

    public int count_stu_warning (){
        return jdbcTemplate.queryForObject("select count(*) from stu_warning", Integer.class);
    }

    public int updateWarning(int id, String condition1, String condition2, String condition3,String condition1ex,String condition2ex,String condition3ex){
        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from stu_warning where studentId=?",id);
        if(list.isEmpty()) {
            return -1;
        }

        String sql = "update stu_warning set warningCondition1 = ?, warningCondition2 = ?, warningCondition3 = ?,warningConditionEx1=?,warningConditionEx2=?,warningConditionEx3=? where studentId = ?";
        jdbcTemplate.update(sql, new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1, condition1);
                preparedStatement.setString(2, condition2);
                preparedStatement.setString(3, condition3);
                preparedStatement.setString(4,condition1ex);
                preparedStatement.setString(5,condition2ex);
                preparedStatement.setString(6,condition3ex);
                preparedStatement.setInt(7, id);
            }
        });
        return 1;
    }
    @Deprecated
    public Map<String,Object> isWarning(List<Map<String,Object>> ans,int studentId){
        Map<String,Object> mm = new HashMap<>();
        List<Map<String,Object>> l = jdbcTemplate.queryForList("select * from watch_student where studentId=?",studentId);
        if(l.isEmpty()){
            mm.put("level",-1);
            return mm;
        }
        else{
            mm.put("name",l.get(0).get("studentName"));
        }
        List<Map<String,Object>> warnings = jdbcTemplate.queryForList("select * from stu_warning where studentId=?",studentId);
        if(warnings.isEmpty()){
            mm.put("level",0);
            return mm;
        }
        int flag = 0;
        for(Map<String,Object> war:warnings){//条目
            JSONArray warningCondition1 = JSON.parseObject((String)war.get("warningCondition1")).getJSONArray("question");
            Map<Integer,Integer> war1 = new HashMap<>();
            for(int i=0;i<warningCondition1.size();i++){
                JSONObject o = warningCondition1.getJSONObject(i);
                int id = (int)o.get("id");
                int answer = (int)o.get("ans");
                war1.put(id,answer);
            }
            flag = 1;
            for(Map<String,Object> m:ans){
                int id = (int)m.get("questionId");
                int answer = (int)m.get("answer");
                if (war1.get(id)!=null && war1.get(id)==answer){

                }
                else{
                    flag=0;
                    break;
                }
            }
            if(flag==1){
                mm.put("level",flag);
                mm.put("tel",((Map<String,Object>)war.get("warningCondition1")).get("reporter"));
                System.out.println("触发");
                return mm;
            }

            JSONArray warningCondition2 = JSON.parseObject((String)war.get("warningCondition2")).getJSONArray("question");
            Map<Integer,Integer> war2 = new HashMap<>();
            for(int i=0;i<warningCondition2.size();i++){
                JSONObject o = JSON.parseObject((String)warningCondition2.get(i));
                int id = (int)o.get("id");
                int answer = (int)o.get("answer");
                war2.put(id,answer);
            }
            flag = 2;
            for(Map<String,Object> m:ans){
                int id = (int)m.get("questionId");
                int answer = (int)m.get("answer");
                if (war1.get(id)!=null && war1.get(id)==answer){

                }
                else{
                    flag=0;
                    break;
                }
            }
            if(flag==2){
                mm.put("level",flag);
                mm.put("tel",((Map<String,Object>)war.get("warningCondition2")).get("reporter"));
                return mm;
            }

            JSONArray warningCondition3 = JSON.parseObject((String)war.get("warningCondition3")).getJSONArray("question");
            Map<Integer,Integer> war3 = new HashMap<>();
            for(int i=0;i<warningCondition3.size();i++){
                JSONObject o = JSON.parseObject((String)warningCondition3.get(i));
                int id = (int)o.get("id");
                int answer = (int)o.get("ans");
                war3.put(id,answer);
            }
            flag = 3;
            for(Map<String,Object> m:ans){
                int id = (int)m.get("questionId");
                int answer = (int)m.get("answer");
                if (war1.get(id)!=null && war1.get(id)==answer){

                }
                else{
                    flag=0;
                    break;
                }
            }
            if(flag==3){
                mm.put("level",flag);
                mm.put("tel",((Map<String,Object>)war.get("warningCondition3")).get("reporter"));
                return mm;
            }
        }
        mm.put("level",flag);
        return mm;
    }

    public List<Map<String, Object>> getWarningStuDetail(int id){
        String sql ="select * from stu_warning where studentId=? and is_ban=0";
        return  jdbcTemplate.queryForList(sql, id);
    }

    public JSONObject getReportersQuestions(int studentId){
        List<Map<String, Object>> reporter = jdbcTemplate.queryForList("select reId as id, reporterRole as role, reporterName as name, exam from reporter_student_table where studentId = ?", studentId);
        JSONArray list_reporter = new JSONArray(), list_question = new JSONArray();
        for (Map<String, Object> map : reporter){
            JSONObject ele = new JSONObject();
            ele.put("id", map.get("id"));
            String tmp = map.get("exam").toString();
            String[] exam =  tmp.substring(1, tmp.length()-1).split(",");

            for (String s:exam ){
                if (s.equals(""))break;
                List<Map<String, Object>> question = jdbcTemplate.queryForList("select questionBody as title, questionId as id, choices as options from question where questionId = ?", Integer.parseInt(s));
                if (!question.isEmpty()){
                    JSONObject ele_1 = new JSONObject();
                    ele_1.put("title", question.get(0).get("title"));
                    ele_1.put("id", question.get(0).get("id"));
                    ele_1.put("options", JSON.parse(question.get(0).get("options").toString()));
                    if (!list_question.contains(ele_1))list_question.add(ele_1);
                }
            }
        }
        List<Map<String, Object>> all_reporters = jdbcTemplate.queryForList("select userid as id, username as name from user_table");

        JSONObject ret = new JSONObject();
        ret.put("question", list_question);
        ret.put("reporter", all_reporters);
        return ret;
    }

    public String getWarningRecord(int studentId,int level){//返回是否有大于等于level的studentId预警记录
        try {
            return jdbcTemplate.queryForObject("select * from warning_record where studentId=? and level>=? and warningTime between current_date and date_add(current_date,interval 1 day)", new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return "";
                }
            },studentId,level);
        }catch (Exception e){
            return null;
        }
    }

    public int doWarning(int studentId,int level){//添加级别为level的预警记录
        return jdbcTemplate.update("insert into warning_record(studentId, level) VALUE (?,?)",studentId,level);
    }

    public int solveLog(int studentid, int logid){
        return jdbcTemplate.update("update warning_record set solved = 1 where studentId=? and warningId=?",studentid,logid);
    }

    public List<Map<String,Object>> getUnsolvedWarning(){
        return jdbcTemplate.queryForList("select * from warning_record where solved=0");
    }
}
