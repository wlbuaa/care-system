package com.buaa.concernandlove.dao;

import com.alibaba.fastjson.JSON;
import com.buaa.concernandlove.Service.EmailService;
import com.buaa.concernandlove.Service.ScheduleService;
import com.buaa.concernandlove.utils.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ChartDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    EmailService emailService;

    public Map<String, Object> getChartManage() {
        Map<String, Object> map_query;
        map_query = jdbcTemplate.queryForMap("select level1_count, level2_count, level1_time, level2_time, range_time, mail," +
                "catch_count,catch_time,phone,time_interval from chart_manage");

        Map<String, Object> map = new HashMap<>();
        map.put("level1_count", map_query.get("level1_count"));
        map.put("level2_count", map_query.get("level2_count"));
        map.put("range_time", map_query.get("range_time"));
        try {
            map.put("mail", map_query.get("mail").toString().split(","));
        }
        catch (Exception e){
            map.put("mail","");
        }
        map.put("level1_time", JsonUtil.jsonToList((String) map_query.get("level1_time")));
        map.put("level2_time", JsonUtil.jsonToList((String) map_query.get("level2_time")));

        map.put("catch_count", map_query.get("catch_count"));
        map.put("catch_time", JsonUtil.jsonToList((String) map_query.get("catch_time")));
        map.put("phone", map_query.get("phone"));
        map.put("interval", map_query.get("time_interval"));
        return map;
    }

    public int setChartManage(int level1_count, int level2_count, List<String> level1_time, List<String> level2_time,
                              int range_time, String mail,
                              int catch_count, List<String> catch_time, String phone, int time_interval) {
        String level1_time_string = JSON.toJSONString(level1_time);
        String level2_time_string = JSON.toJSONString(level2_time);
        String catch_time_string = JSON.toJSONString(catch_time);
        int result = jdbcTemplate.update("update chart_manage set level1_count=?,level2_count=?,level1_time=?,level2_time=?,range_time=?,mail=?,catch_count=?,catch_time=?,phone=?,time_interval=? where id=1",
                level1_count, level2_count, level1_time_string, level2_time_string, range_time, mail,
                catch_count, catch_time_string, phone, time_interval);
        return result;
    }
}
