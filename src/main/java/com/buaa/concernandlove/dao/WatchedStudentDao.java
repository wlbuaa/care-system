package com.buaa.concernandlove.dao;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.buaa.concernandlove.pojo.Couriers;
import com.buaa.concernandlove.pojo.WatchedStudent;
import com.buaa.concernandlove.pojo.WatchedStudentDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class WatchedStudentDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    public int addWatchedStudent(WatchedStudentDetail watchedStudent){
        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from watch_student where stuId=?",watchedStudent.getStuId());
        if(!list.isEmpty()) {
            return -1;
        }
        String sql1="insert into watch_student(studentName, phoneNum, stuId, classNum, grade, studentAvatarUrl) VALUE (?,?,?,?,?,?)";
        KeyHolder keyHolder=new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement=con.prepareStatement(sql1,PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1,watchedStudent.getStudentName());
                preparedStatement.setString(2,watchedStudent.getPhoneNum());
                preparedStatement.setString(3,watchedStudent.getStuId());
                preparedStatement.setString(4,watchedStudent.getClassNum());
                preparedStatement.setString(5,watchedStudent.getGrade());
                preparedStatement.setString(6,watchedStudent.getStudentAvatarUrl());
                return preparedStatement;
            }
        },keyHolder);
        int studentId = keyHolder.getKey().intValue();
        String sql2 = "insert into watch_student_detail" +
                "(studentId, studentName, phoneNum, stuId, classNum, dormNum, sex, homeAddress, grade, classMonitorName, classMonitorPhoneNum, mentorName, " +
                "mentorPhoneNum, emergencyContactName, emergencyContactPhoneNum, counselorName, counselorPhoneNum, concernLevel, concernReason, specialCondition, studentAvatarUrl) " +
                "VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement=con.prepareStatement(sql2);
                preparedStatement.setInt(1,studentId);
                preparedStatement.setString(2,watchedStudent.getStudentName());
                preparedStatement.setString(3,watchedStudent.getPhoneNum());
                preparedStatement.setString(4,watchedStudent.getStuId());
                preparedStatement.setString(5,watchedStudent.getClassNum());
                preparedStatement.setString(6,watchedStudent.getDormNum());
                preparedStatement.setString(7,watchedStudent.getSex());
                preparedStatement.setString(8,watchedStudent.getHomeAddress());
                preparedStatement.setString(9,watchedStudent.getGrade());
                preparedStatement.setString(10,watchedStudent.getClassMonitorName());
                preparedStatement.setString(11,watchedStudent.getClassMonitorPhoneNum());
                preparedStatement.setString(12,watchedStudent.getMentorName());
                preparedStatement.setString(13,watchedStudent.getMentorPhoneNum());
                preparedStatement.setString(14,watchedStudent.getEmergencyContactName());
                preparedStatement.setString(15,watchedStudent.getEmergencyContactPhoneNum());
                preparedStatement.setString(16,watchedStudent.getCounselorName());
                preparedStatement.setString(17,watchedStudent.getCounselorPhoneNum());
                preparedStatement.setString(18,watchedStudent.getConcernLevel());
                preparedStatement.setString(19,watchedStudent.getConcernReason());
                preparedStatement.setString(20,watchedStudent.getSpecialCondition());
                preparedStatement.setString(21,watchedStudent.getStudentAvatarUrl());
                return preparedStatement;
            }
        });
        String sql3 = "insert into reporter_student_table (stuId, reporterName, reporterRole, startTime, endTime, frequency, exam, reporterId, studentId,startTimeBackUp,endTimeBackUp) VALUE (?,?,?,?,?,?,?,?,?,?,?)";
        for(Couriers couriers:watchedStudent.getCouriersList()){
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement preparedStatement=con.prepareStatement(sql3);
                    preparedStatement.setString(1,watchedStudent.getStuId());
                    preparedStatement.setString(2,couriers.getName());
                    preparedStatement.setInt(3,couriers.getRole());
                    preparedStatement.setTimestamp(4,new Timestamp(couriers.getStartTime()));
                    preparedStatement.setTimestamp(5,new Timestamp(couriers.getEndTime()));
                    preparedStatement.setString(6,couriers.getFrequency());
                    preparedStatement.setString(7, JSON.toJSONString(couriers.getExamId()));
                    preparedStatement.setString(8,couriers.getReporterId());
                    preparedStatement.setInt(9,studentId);
                    preparedStatement.setTimestamp(10,new Timestamp(couriers.getStartTime()));
                    preparedStatement.setTimestamp(11,new Timestamp(couriers.getEndTime()));
                    return preparedStatement;
                }
            });
            jdbcTemplate.update("update reporter_student_table join user_table u on u.userStuId=reporter_student_table.reporterId set reId=userid where studentId=? and reporterId=?",studentId,couriers.getReporterId());
        }
        return studentId;
    }
    public List<Map<String,Object>> getWatchedStudentById(int id){
        return jdbcTemplate.queryForList("select * from watch_student join watch_student_detail wsd on watch_student.studentId = wsd.studentId where watch_student.studentId=?",id);
    }
    public List<Map<String,Object>> getCouriersById(int id){
        return jdbcTemplate.queryForList("select reporterName as name,reporterRole as role,startTime as start,endTime as end,frequency,exam,reporterId as `num`,startTimeBackUp `startTimeBackUp`,endTimeBackUp as `endTimeBackUp` from reporter_student_table where studentId=?",id);
    }
    public List<Map<String,Object>> getCouriesByIdNewName(int id){
        return jdbcTemplate.queryForList("select reporterName as name,reporterRole as role,startTime as start,endTime as end,frequency,exam,reporterId as `reporterId` from reporter_student_table where studentId=?",id);

    }
    public List<Map<String,Object>> getWatchedStudentById(String stuId){
        return jdbcTemplate.queryForList("select * from watch_student join watch_student_detail wsd on watch_student.studentId = wsd.studentId where watch_student.stuId=?",stuId);
    }
    public List<Map<String,Object>> getAllStudent(){
        return jdbcTemplate.queryForList("select studentAvatarUrl as photo,studentName as name,stuId as num,phoneNum as tel,classNum as class,isStopped as stopped,isReported as reported from watch_student");
    }
    public List<Map<String,Object>> getAllStudentByPage(int pageNum, int pageSize) {
        return jdbcTemplate.queryForList("select studentId as id,studentAvatarUrl as photo,studentName as name,stuId as num,phoneNum as tel,classNum as class,isStopped as stopped,isReported as reported from watch_student limit "+(pageNum-1)*pageSize+","+pageSize);
    }
    public int getAllStudentPageNum() {
        return jdbcTemplate.queryForObject("select count(*) from watch_student",Integer.class);
    }
    public List<Map<String,Object>> getKeyStudentByPage(int pageNum, int pageSize, String name) {
        return jdbcTemplate.queryForList("select studentId as id,studentAvatarUrl as photo,studentName as name,stuId as num,phoneNum as tel,classNum as class,isStopped as stopped,isReported as reported from watch_student where studentName like ? limit "+(pageNum-1)*pageSize+","+pageSize,"%"+name+"%");
    }
    public int getKeyStudentPageNum(String name) {
        return jdbcTemplate.queryForObject("select count(*) from watch_student where studentName=?",Integer.class,name);
    }
    public int updateStuInfo(WatchedStudentDetail watchedStudentDetail){
        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from watch_student where studentId=?",watchedStudentDetail.getStudentId());
        if(list.isEmpty()) {
            return -1;
        }
        jdbcTemplate.update("update watch_student set studentName=?,phoneNum=?,stuId=?,classNum=?,grade=?,studentAvatarUrl=? where studentId=?",watchedStudentDetail.getStudentName(),watchedStudentDetail.getPhoneNum(),watchedStudentDetail.getStuId(),watchedStudentDetail.getClassNum(),watchedStudentDetail.getGrade(),watchedStudentDetail.getStudentAvatarUrl(),watchedStudentDetail.getStudentId());
        jdbcTemplate.update("update watch_student_detail set studentName=?,phoneNum=?,stuId=?,classNum=?,grade=?,dormNum=?,sex=?,homeAddress=?,classMonitorName=?,\n" +
                        "classMonitorPhoneNum=?,mentorName=?,mentorPhoneNum=?,emergencyContactName=?,emergencyContactPhoneNum=?,counselorName=?,counselorPhoneNum=?,\n" +
                        "concernLevel=?,concernReason=?,specialCondition=?,studentAvatarUrl=? where studentId=?",watchedStudentDetail.getStudentName(),watchedStudentDetail.getPhoneNum(),watchedStudentDetail.getStuId(),watchedStudentDetail.getClassNum(),watchedStudentDetail.getGrade(),
                watchedStudentDetail.getDormNum(),watchedStudentDetail.getSex(),watchedStudentDetail.getHomeAddress(),watchedStudentDetail.getClassMonitorName(),watchedStudentDetail.getClassMonitorPhoneNum(),
                watchedStudentDetail.getMentorName(),watchedStudentDetail.getMentorPhoneNum(),watchedStudentDetail.getEmergencyContactName(),watchedStudentDetail.getEmergencyContactPhoneNum(),
                watchedStudentDetail.getCounselorName(),watchedStudentDetail.getCounselorPhoneNum(),watchedStudentDetail.getConcernLevel(),watchedStudentDetail.getConcernReason(),watchedStudentDetail.getSpecialCondition(),watchedStudentDetail.getStudentAvatarUrl(),watchedStudentDetail.getStudentId());
        jdbcTemplate.update("delete from reporter_student_table where studentId=?",watchedStudentDetail.getStudentId());
        String sql3 = "insert into reporter_student_table (stuId, reporterName, reporterRole, startTime, endTime, frequency, exam, reporterId, studentId,startTimeBackUp,endTimeBackUp) VALUE (?,?,?,?,?,?,?,?,?,?,?)";
        for(Couriers couriers:watchedStudentDetail.getCouriersList()){
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement preparedStatement=con.prepareStatement(sql3);
                    preparedStatement.setString(1,watchedStudentDetail.getStuId());
                    preparedStatement.setString(2,couriers.getName());
                    preparedStatement.setInt(3,couriers.getRole());
                    preparedStatement.setTimestamp(4,new Timestamp(couriers.getStartTime()));
                    preparedStatement.setTimestamp(5,new Timestamp(couriers.getEndTime()));
                    preparedStatement.setString(6,couriers.getFrequency());
                    preparedStatement.setString(7, JSON.toJSONString(couriers.getExamId()));
                    preparedStatement.setString(8,couriers.getReporterId());
                    preparedStatement.setInt(9,watchedStudentDetail.getStudentId());
                    preparedStatement.setTimestamp(10,new Timestamp(couriers.getStartTime()));
                    preparedStatement.setTimestamp(11,new Timestamp(couriers.getEndTime()));
                    return preparedStatement;
                }
            });
            jdbcTemplate.update("update reporter_student_table join user_table u on u.userStuId=reporter_student_table.reporterId set reId=userid where studentId=? and reporterId=?",watchedStudentDetail.getStudentId(),couriers.getReporterId());
        }
//        for(Couriers couriers:watchedStudentDetail.getCouriersList()){
//            if(jdbcTemplate.queryForList("select * from reporter_student_table where studentId=? and reporterId=?",watchedStudentDetail.getStudentId(),couriers.getReporterId()).isEmpty()){
//                jdbcTemplate.update(new PreparedStatementCreator() {
//                    @Override
//                    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
//                        PreparedStatement preparedStatement=con.prepareStatement("insert into reporter_student_table (stuId, reporterName, reporterRole, startTime, endTime, frequency, exam, reporterId, studentId,startTimeBackUp,endTimeBackUp) VALUE (?,?,?,?,?,?,?,?,?,?,?)");
//                        preparedStatement.setString(1,watchedStudentDetail.getStuId());
//                        preparedStatement.setString(2,couriers.getName());
//                        preparedStatement.setInt(3,couriers.getRole());
//                        preparedStatement.setTimestamp(4,new Timestamp(couriers.getStartTime()));
//                        preparedStatement.setTimestamp(5,new Timestamp(couriers.getEndTime()));
//                        preparedStatement.setString(6,couriers.getFrequency());
//                        preparedStatement.setString(7, JSON.toJSONString(couriers.getExamId()));
//                        preparedStatement.setString(8,couriers.getReporterId());
//                        preparedStatement.setInt(9,watchedStudentDetail.getStudentId());
//                        preparedStatement.setTimestamp(10,new Timestamp(couriers.getStartTime()));
//                        preparedStatement.setTimestamp(11,new Timestamp(couriers.getEndTime()));
//                        return preparedStatement;
//                    }
//                });
//                jdbcTemplate.update("update reporter_student_table join user_table u on u.userStuId=reporter_student_table.reporterId set reId=userid where studentId=?",watchedStudentDetail.getStudentId());
//            }
//            else{
//                jdbcTemplate.update("update reporter_student_table set stuId=?,reporterName=?,reporterRole=?,startTime=?,endTime=?,frequency=?,exam=?,startTimeBackUp=?,endTimeBackUp=? where studentId=? and reporterId=?",
//                        watchedStudentDetail.getStuId(),couriers.getName(),couriers.getRole(),new Timestamp(couriers.getStartTime()),new Timestamp(couriers.getEndTime()),couriers.getFrequency(),
//                        couriers.getExamId().toJSONString(),new Timestamp(couriers.getStartTime()),new Timestamp(couriers.getEndTime()),watchedStudentDetail.getStudentId(),couriers.getReporterId());
//                jdbcTemplate.update("update reporter_student_table join user_table u on u.userStuId=reporter_student_table.reporterId set reId=userid where studentId=?",watchedStudentDetail.getStudentId());
//            }
//        }
        return 1;
    }
    public int deleteStudent(int id){
        List<Map<String,Object>> list = jdbcTemplate.queryForList("select * from watch_student where studentId=?",id);
        if(list.isEmpty()) {
            return -1;
        }
        jdbcTemplate.update("delete from watch_student where studentId=?",id);
        jdbcTemplate.update("delete from watch_student_detail where studentId=?",id);
        jdbcTemplate.update("delete from reporter_student_table where studentId=?",id);
        jdbcTemplate.update("delete from stu_warning where studentId=?",id);
        return 1;
    }

    public int trackStudent(int studentId) {
        return jdbcTemplate.update("update watch_student set isStopped=0 where studentId=?", studentId);
    }
    public int untrackStudent(int studentId) {
        return jdbcTemplate.update("update watch_student set isStopped=1 where studentId=?", studentId);
    }
    public void setReported0(){
        jdbcTemplate.update("update watch_student set isReported=0");
    }

    public void setReported0(int watchedStudentId){
        jdbcTemplate.update("update watch_student set isReported=0 where studentId=?",watchedStudentId);
    }
    public void setReported1(int watchedStudentId) {
        jdbcTemplate.update("update watch_student set isReported=1 where studentId=?",watchedStudentId);
    }
    public List<Map<String,Object>> getReportMessage(String where,String orderBy){
        return jdbcTemplate.queryForList("select userStuId as `courier_num`,reportTime as `time`,recordId as `id` from report_record join user_table on report_record.reporterId=userid "+where+orderBy);
    }
    public int getReportMessageTotalNum(String where) {
        return jdbcTemplate.queryForObject("select count(*) from report_record "+where,Integer.class);
    }

    public List<Map<String, Object>> getWatchedStudentList(String userStuId) {
        return jdbcTemplate.queryForList("select watch_student.stuId as `studentId`,isStopped as `stop`,studentName as `name`,count(if(rr.reportTime between current_date and date_add(current_date,interval 1 day ),1,null)) as `reported` from (select stuId from reporter_student_table where reporterId=?)as p join watch_student on p.stuId=watch_student.stuId left join report_record rr on watch_student.studentId = rr.studentId group by studentName, watch_student.stuId",userStuId);
    }
    public List<Map<String,Object>> getWatchStudentByBoth(String reporterId,String studentId){
        return jdbcTemplate.queryForList("select * from reporter_student_table where reporterId=? and stuId=?",reporterId,studentId);
    }

    public void dailyUpdateIsReportedOfWatchedStudent(){
        jdbcTemplate.update("update watch_student as ws " +
                "set isReported=0 " +
                "where exists(" +
                "select * from reporter_student_table as rst " +
                "where ws.studentId=rst.studentId and rst.frequency='每天一次')");
    }

    public void weeklyUpdateIsReportedOfWatchedStudent(){
        jdbcTemplate.update("update watch_student as ws " +
                "set isReported=0 " +
                "where not exists(" +
                "select * from reporter_student_table as rst1 " +
                "where ws.studentId=rst1.studentId and rst1.frequency='每天一次') " +
                "and exists(" +
                "select * from reporter_student_table as rst2 " +
                "where ws.studentId=rst2.studentId and rst2.frequency='每周一次')");
    }

    public void monthlyUpdateIsReportedOfWatchedStudent(){
        jdbcTemplate.update("update watch_student as ws " +
                "set isReported=0 " +
                "where not exists(" +
                "select * from reporter_student_table as rst1 " +
                "where ws.studentId=rst1.studentId and rst1.frequency='每天一次') " +
                "and not exists( " +
                "select * from reporter_student_table as rst2 " +
                "where ws.studentId=rst2.studentId and rst2.frequency='每周一次') " +
                "and exists " +
                " select * from reporter_student_table as rst3 " +
                "where ws.studentId=rst3.studentId and rst3.frequency='每月一次')");
    }
    public Map<String,Object> getQAndAById(int id){
        Map<String,Object> map = new HashMap<>();
        List<Map<String,Object>> reporter = jdbcTemplate.queryForList("select userStuId as num,username as name,reportTime from report_record join user_table u on u.userid=report_record.reporterId where recordId=?",id);
        if (reporter.isEmpty()){
            map.put("success",false);
            map.put("exc","id不存在");
            map.put("id",id);
            return map;
        }
        map = reporter.get(0);
        List<Map<String,Object>> answers = jdbcTemplate.queryForList("select answer_record.questionId,type,questionBody,isMust,choices,answer from answer_record join question q on answer_record.questionId = q.questionId where reportRecordId=?",id);
        for(Map<String,Object> m:answers){
            m.put("choices",JSON.parseArray((String) m.get("choices")));
        }
        map.put("answers",answers);
        map.put("success",true);
        map.put("exc","");
        map.put("id",id);
        return map;
    }
    public Timestamp getStartTime(String studentStuId,int reporterId){
        return jdbcTemplate.queryForObject("select startTime from reporter_student_table where stuId=? and reId=?", new RowMapper<Timestamp>() {
            @Override
            public Timestamp mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getTimestamp("startTime");
            }
        }, studentStuId, reporterId);
    }

    public Timestamp getEndTIme(String studentStuId, int userid) {
        return jdbcTemplate.queryForObject("select endTime from reporter_student_table where stuId=? and reId=?", new RowMapper<Timestamp>() {
            @Override
            public Timestamp mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getTimestamp("endTime");
            }
        }, studentStuId, userid);
    }

    public List<Map<String, Object>> getConvRecord(String id) {
        return jdbcTemplate.queryForList("select upTime as `time`,address as `location` from conv_record where stuId=?",id);
    }

    public List<Map<String, Object>> getConvRecordByDay(String id, Calendar day) {
        System.out.println(day.getTime());

        Calendar nextday = Calendar.getInstance();
        nextday.setTime(day.getTime());
        nextday.add(Calendar.DATE, 1);

        Timestamp start = new Timestamp(day.getTimeInMillis());
        Timestamp end = new Timestamp(nextday.getTimeInMillis());

        return jdbcTemplate.queryForList("select upTime as `report_time`," +
                "address as `report_location` from conv_record where stuId=? " +
                "and upTime>? and upTime<?",id, start, end);
    }
}
