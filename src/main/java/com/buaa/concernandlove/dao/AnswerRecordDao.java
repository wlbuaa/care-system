package com.buaa.concernandlove.dao;

import com.buaa.concernandlove.pojo.AnswerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AnswerRecordDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    public int addAnswerRecord(AnswerRecord answerRecord){
        return jdbcTemplate.update("insert into answer_record(reportRecordId, questionId,  answer) VALUE (?,?,?)",answerRecord.getReportRecordId(),answerRecord.getQuestionId(),answerRecord.getAnswer());
    }

    public List<Map<String,Object>> getAllAnswerByReportId(int reportRecordId){
        return jdbcTemplate.queryForList("select * from answer_record where reportRecordId=?",reportRecordId);
    }
}
