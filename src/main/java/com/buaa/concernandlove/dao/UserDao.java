package com.buaa.concernandlove.dao;

import com.buaa.concernandlove.pojo.User;
import com.buaa.concernandlove.utils.CalendarUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Repository
public class UserDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    public int addUser(User user){
        String sql="insert into user_table(userStuId, username, userAvatarUrl, userPassword, userPhoneNumber, userRole,userPasswordRaw) VALUE (?,?,?,?,?,?,?)";
        KeyHolder keyHolder=new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement=con.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1,user.getUserStuId());
                preparedStatement.setString(2,user.getUsername());
                preparedStatement.setString(3,"/userDefaultAvatar.png");
                preparedStatement.setString(4,user.getUserPassword());
                preparedStatement.setString(5,user.getUserPhoneNumber());
                preparedStatement.setString(6,user.getUserRole());
                preparedStatement.setString(7,user.getUserPasswordRaw());
                return preparedStatement;
            }
        },keyHolder);
        return keyHolder.getKey().intValue();
    }
    public User getUserById(int userid){
        try {
            return (User) jdbcTemplate.queryForObject("select * from user_table where userid=?",new UserRowMapper(),userid);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
    }
    public User getUserByStuId(String stuId){
        try {
            return (User) jdbcTemplate.queryForObject("select * from user_table where binary userStuId=?",new UserRowMapper(),stuId);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
    }

    public List<Map<String,Object>> getReporterByPage(int pageNum, int pageSize) {
        return jdbcTemplate.queryForList("select username as `name`,userid as `id`,userPhoneNumber as `phone`,userPasswordRaw as `password`,is_ban as `is_ban`,userStuId as `userStuId` from user_table where userRole!='admin' limit "+(pageNum-1)*pageSize+","+pageSize);
    }

    public int getReporterPageNum() {
        return jdbcTemplate.queryForObject("select count(*) from user_table where userRole!='admin'",Integer.class);
    }

    public void updateUser(User user) {
        jdbcTemplate.update("update user_table set userPassword=?,userPasswordRaw=?,userPhoneNumber=?,username=? where userStuId=?",user.getUserPassword(),user.getUserPasswordRaw(),user.getUserPhoneNumber(),user.getUsername(),user.getUserStuId());
    }

    public List<Map<String, Object>> searchReporterByPage(String where, String limit) {
        return jdbcTemplate.queryForList("select username as `name`,userid as `id`,userPhoneNumber as `phone`,userPasswordRaw as `password`,is_ban as `is_ban` from user_table "+where+limit);
    }

    public int getSearchResultNum(String where) {
        return jdbcTemplate.queryForObject("select count(*) from user_table "+where,Integer.class);
    }

    public void deleteReporterById(int id) {
        jdbcTemplate.update("delete from user_table where userid=?",id);
    }

    public void banReporterById(int id) {
        jdbcTemplate.update("update user_table set is_ban=true where userid=?",id);
    }

    public List<User> getReportersByWatchedStudentId(String watchedStuId){
        try {
            String sql="select reporterId from reporter_student_table rst " +
                    "join user_table ut on rst.reId=ut.userid where rst.stuId=? and ut.is_ban=0";
            List<String> reporterIdList= jdbcTemplate.query(sql, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet resultSet, int i) throws SQLException {
                    return resultSet.getString("reporterId");
                }
            }, watchedStuId);
            List<User> userList=new ArrayList<>();
            for(String reportedId:reporterIdList){
                userList.add(getUserByStuId(reportedId));
            }
            return userList;
        }
        catch (Exception e){
            return null;
        }
    }

    public int countReportersByWatchedStudentId(int watchedStudentId){
        return jdbcTemplate.queryForObject("select count(*) from reporter_student_table rst " +
                "join user_table ut on rst.reId=ut.userid where rst.studentId=? and ut.is_ban=0",Integer.class,watchedStudentId);
    }
    public void cancelBanById(int id) {
        jdbcTemplate.update("update user_table set is_ban=false where userid=?",id);
    }

    public List<Map<String,Object>>  getAllReporterInfo(){
        return jdbcTemplate.queryForList("select * from reporter_student_table rst join user_table ut " +
                "on rst.reId=ut.userid and ut.is_ban=0");
    }

    public List<Map<String,Object>> getReporterByStudentIdAndReporterId(int reporterId,int studentId){
        return jdbcTemplate.queryForList("select * from reporter_student_table where " +
                "reId=? and studentId=?",reporterId,studentId);
    }

    public String getReporterNameById(int reporterId) {
        return jdbcTemplate.queryForList("select reporterName from reporter_student_table" +
                " where reId=?",String.class, reporterId).get(0);
    }

    public void dailyUpdateStartAndEndOfReporters(){
        String frequency="每天一次";
        List<Map<String,Object>> reporterList=jdbcTemplate.queryForList("select * from reporter_student_table " +
                "where frequency=?",frequency);
        for(Map<String,Object> reporter:reporterList){
            int reporterId=Integer.parseInt(reporter.get("reId").toString());
            int studentId=Integer.parseInt(reporter.get("studentId").toString());
            Timestamp start=(Timestamp)reporter.get("startTime");
            Timestamp end=(Timestamp)reporter.get("endTime");
            Timestamp updatedStart=CalendarUtil.getNowDayTimestamp(start);
            Timestamp updatedEnd=CalendarUtil.getNowDayTimestamp(end);
            System.out.println(updatedStart);
            System.out.println(updatedEnd);
            jdbcTemplate.update("update reporter_student_table " +
                    "set startTime=? , endTime=? " +
                    "where reId=? and studentId=?",updatedStart,updatedEnd,reporterId,studentId);
        }
    }

    public void weeklyUpdateStartAndEndOfReporters(){
        String frequency="每周一次";
        List<Map<String,Object>> reporterList=jdbcTemplate.queryForList("select * from reporter_student_table " +
                "where frequency=?",frequency);
        for(Map<String,Object> reporter:reporterList){
            int reporterId=Integer.parseInt(reporter.get("reId").toString());
            int studentId=Integer.parseInt(reporter.get("studentId").toString());
            Timestamp start=(Timestamp)reporter.get("startTime");
            Timestamp end=(Timestamp)reporter.get("endTime");
            Timestamp updatedStart=CalendarUtil.getNowWeekTimestamp(start);
            Timestamp updatedEnd=CalendarUtil.getNowWeekTimestamp(end);
            System.out.println(updatedEnd);
            System.out.println(updatedStart);
            jdbcTemplate.update("update reporter_student_table " +
                    "set startTime=? , endTime=? " +
                    "where reId=? and studentId=?",updatedStart,updatedEnd,reporterId,studentId);
        }
    }

    public void monthlyUpdateStartAndEndOfReporters(){
        String frequency="每月一次";
        List<Map<String,Object>> reporterList=jdbcTemplate.queryForList("select * from reporter_student_table " +
                "where frequency=?",frequency);
        for(Map<String,Object> reporter:reporterList){
            int reporterId=Integer.parseInt(reporter.get("reId").toString());
            int studentId=Integer.parseInt(reporter.get("studentId").toString());
            Timestamp start=(Timestamp)reporter.get("startTime");
            Timestamp end=(Timestamp)reporter.get("endTime");
            Timestamp updatedStart=CalendarUtil.getNowMonthTimestamp(start);
            Timestamp updatedEnd=CalendarUtil.getNowMonthTimestamp(end);
            jdbcTemplate.update("update reporter_student_table " +
                    "set startTime=? , endTime=? " +
                    "where reId=? and studentId=?",updatedStart,updatedEnd,reporterId,studentId);
        }
    }
    private static class UserRowMapper implements RowMapper{

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user=new User();
            user.setUsername(rs.getString("username"));
            user.setUserAvatarUrl(rs.getString("userAvatarUrl"));
            user.setBan(rs.getInt("is_ban")==1);
            user.setUserPassword(rs.getString("userPassword"));
            user.setUserPasswordRaw(rs.getString("userPasswordRaw"));
            user.setUserPhoneNumber(rs.getString("userPhoneNumber"));
            user.setUserid(rs.getInt("userid"));
            user.setUserRole(rs.getString("userRole"));
            user.setUserStuId(rs.getString("userStuId"));
            return user;
        }
    }
}
