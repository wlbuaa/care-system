package com.buaa.concernandlove.dao;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.buaa.concernandlove.pojo.ReportRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class ReportDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public String findQuestionAndAnswer(int reportId) {
        String res = "";
        // reportRecordId, questionId, answer
        List<Map<String, Object>> list = jdbcTemplate.queryForList("select * from answer_record" +
                " where reportRecordId=?", reportId);

        int cnt = 0;
        // 遍历此次提交中回答的所有题目记录
        for(Map<String, Object> reportMap : list) {
            // 记录当前的题号
            cnt += 1;

            int questionId = (int) reportMap.get("questionId");

            // question表：题库
            // 找到题库中的对应题目
            Map<String, Object> questionMap = jdbcTemplate.queryForList("select * from question where questionId=?",
                    questionId).get(0);

            //questionBody, choices
            // 输出题干
            res += String.format("%d. %s\n", cnt, (String) questionMap.get("questionBody"));

            String[] optionLabel = {"A", "B", "C", "D", "E", "F", "G"};
            String choicesJson = (String) questionMap.get("choices");
            JSONArray jsonArray = JSON.parseArray(choicesJson);

            // 输出选项
            String optionText = "";
            for(int i = 0; i < jsonArray.size(); i++) {
                String text = jsonArray.getString(i);
                optionText += String.format("%s. %s   ", optionLabel[i], text);
            }
            res += (optionText + "\n");

            // 输出答案
            int answer = (int) reportMap.get("answer");
            res += String.format("选择了： %s\n\n", optionLabel[answer]);
        }
        return res;
    }

    public int addReportRecord(ReportRecord record){
        String sql="insert into report_record(reporterId, studentId, specialCondition) VALUE (?,?,?)";
        KeyHolder keyHolder=new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement=con.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1,record.getReporterId());
                preparedStatement.setInt(2,record.getStudentId());
                preparedStatement.setString(3,record.getSpecialCondition());
                return preparedStatement;
            }
        },keyHolder);
        return keyHolder.getKey().intValue();
    }

    public ReportRecord findTodayReportRecord(int reporterId, int studentId) {
        Date now = new Date();
        Timestamp timestamp1=null;
        Timestamp timestamp2=null;
        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
        String today=sdf.format(now)+" 00:00:00";
        try {
            timestamp1 = Timestamp.valueOf(today);
            timestamp2 = new Timestamp(now.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        try{
            return jdbcTemplate.queryForObject("select * from report_record where reporterId=? and studentId=? and " +
                            "reportTime>? and reportTime<?",
                    new BeanPropertyRowMapper<>(ReportRecord.class),reporterId,studentId,timestamp1,timestamp2);
        }
        catch (Exception e){
            return null;
        }
    }
    public ReportRecord findTodayReportRecord(String reporterId,String studentId){
        try {
            return jdbcTemplate.queryForObject("select * from report_record join user_table on reporterId=userid join watch_student ws on report_record.studentId = ws.studentId where userStuId=? and stuId=? and reportTime between current_date and date_add(current_date,interval 1 day )",new BeanPropertyRowMapper<>(ReportRecord.class),reporterId,studentId);
        }catch (Exception e){
            return null;
        }
    }
    public ReportRecord findThisWeekReportRecord(String reporterId,String studentId){
        try {
            return jdbcTemplate.queryForObject("select * from report_record join user_table on reporterId=userid join watch_student ws on report_record.studentId = ws.studentId where userStuId=? and stuId=? and reportTime between date_sub(curdate(),INTERVAL WEEKDAY(curdate()) + 1 DAY) and date_sub(curdate(),INTERVAL WEEKDAY(curdate()) - 5 DAY)",new BeanPropertyRowMapper<>(ReportRecord.class),reporterId,studentId);
        }catch (Exception e){
            return null;
        }
    }
    public ReportRecord findThisMonthReportRecord(String reporterId,String studentId){
        try {
            return jdbcTemplate.queryForObject("select * from report_record join user_table on reporterId=userid join watch_student ws on report_record.studentId = ws.studentId where userStuId=? and stuId=? and reportTime between date_add(curdate(), interval - day(curdate()) + 1 day) and last_day(curdate())",new BeanPropertyRowMapper<>(ReportRecord.class),reporterId,studentId);
        }catch (Exception e){
            return null;
        }
    }
    public int countReportedReportersByWatchedStudentId(int watchedStudentId){
//        Date now = new Date();
//        Timestamp timestamp1=null;
//        Timestamp timestamp2=null;
//        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
//        String today=sdf.format(now)+" 00:00:00";
//        try {
//            timestamp1 = Timestamp.valueOf(today);
//            timestamp2 = new Timestamp(now.getTime());
//        } catch (Exception e) {
//            e.printStackTrace();
//            return -1;
//        }
//        return jdbcTemplate.queryForObject("select count(*) from report_record where " +
//                " studentId=? and reportTime>? and reportTime<?",Integer.class,watchedStudentId,timestamp1,timestamp2);
        int reportedNum=0;
        List<Map<String,Object>> reporterList=
                jdbcTemplate.queryForList("select * from reporter_student_table rst " +
                        "join user_table ut on rst.reId=ut.userid where rst.studentId=? and ut.is_ban=0",watchedStudentId);
        for(Map<String,Object> reporter:reporterList){
            Timestamp startTime=(Timestamp)reporter.get("startTime");
            Timestamp endTime=new Timestamp(new Date().getTime());
            int reporterId=0;
            try {
                reporterId = (Integer) reporter.get("reId");
            }
            catch (Exception e){
                e.printStackTrace();
                continue;
            }
            int studentId=(Integer)reporter.get("studentId");
            if(findReportBetween(studentId,reporterId,startTime,endTime)!=null){
                reportedNum++;
            }
        }
        return reportedNum;
    }

    public List<ReportRecord> findReportsBetween(int studentId,String start,String end){
        String sql = "select * from report_record where studentId = ? and reportTime>? and reportTime<?";
        Timestamp timestamp1=Timestamp.valueOf(start);
        Timestamp timestamp2=Timestamp.valueOf(end);
        return jdbcTemplate.query(sql, new RowMapper<ReportRecord>() {
            @Override
            public ReportRecord mapRow(ResultSet rs, int rowNum) throws SQLException {
                ReportRecord record=new ReportRecord();
                record.setSpecialCondition(rs.getString("specialCondition"));
                record.setReporterId(rs.getInt("reporterId"));
                record.setReportTime(rs.getTimestamp("reportTime"));
                record.setRecordId(rs.getInt("recordId"));
                record.setStudentId(rs.getInt("studentId"));
                return record;
            }
        }, studentId, timestamp1, timestamp2);
    }

    public ReportRecord findReportBetween(int studentId,int reporterId,Timestamp start,Timestamp end){
        try {
            return jdbcTemplate.queryForObject("select * from report_record where reporterId=? and studentId=? and " +
                            "reportTime>? and reportTime<?",
                    new BeanPropertyRowMapper<>(ReportRecord.class), reporterId, studentId, start, end);
        }
        catch (Exception e){
            return null;
        }
    }

    public List<Map<String,Object>> getStudentAnswerRecord(int studentId){
        return jdbcTemplate.queryForList("select *,count(*) as num from (select * from (select recordId from (select reId from reporter_student_table where studentId=? )as reporter join report_record on reporter.reId=reporterId )as record join answer_record on recordId=reportRecordId and answerTime between current_date and date_add(current_date,interval 1 day ))as s group by s.questionId,s.answer",studentId);
    }
}
