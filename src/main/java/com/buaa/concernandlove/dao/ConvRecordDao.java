package com.buaa.concernandlove.dao;

import io.swagger.models.auth.In;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
public class ConvRecordDao {
    @Autowired
    JdbcTemplate jdbcTemplate;
    public int countMoveTimesBetween(String watchedStudentId,String startStr,String endStr){
        Timestamp timestamp1=Timestamp.valueOf(startStr);
        Timestamp timestamp2=Timestamp.valueOf(endStr);
        return jdbcTemplate.queryForObject("select count(*) from conv_record where stuId=? and upTime>? and " +
                        "upTime<? and isMove=1",
                Integer.class,watchedStudentId,timestamp1,timestamp2);
    }

    public Timestamp getClockTime(String watchedStudentId,String startStr,String endStr) {
        Timestamp timestamp1 = Timestamp.valueOf(startStr);
        Timestamp timestamp2 = Timestamp.valueOf(endStr);
        try {
            Timestamp timestamp = jdbcTemplate.queryForObject("select upTime from conv_record where stuId=? and upTime>? and " +
                            "upTime<?;",
                    Timestamp.class, watchedStudentId, timestamp1, timestamp2);
            return timestamp;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}
