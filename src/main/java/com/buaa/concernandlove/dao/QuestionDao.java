package com.buaa.concernandlove.dao;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.buaa.concernandlove.pojo.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class QuestionDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    public int addQuestion(Question question){
        String sql="insert into question (creatorId, type, questionBody, isMust, choices) values (?,?,?,?,?);";
        KeyHolder keyHolder=new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement preparedStatement=con.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1,question.getCreatorId());
                preparedStatement.setInt(2,question.getType());
                preparedStatement.setString(3,question.getQuestionBody());
                preparedStatement.setInt(4,question.getIsMust());
                preparedStatement.setString(5,question.getChoices().toJSONString());
                return preparedStatement;
            }
        },keyHolder);
        return keyHolder.getKey().intValue();
    }
    public int deleteQuestionById(JSONArray IdArray){
        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate);
        Map<String,Object> paramMap=new HashMap<>();
        paramMap.put("ids",IdArray.toJavaList(Integer.class));
        return namedParameterJdbcTemplate.update("delete from question where questionId in (:ids)",paramMap);
    }
    public List<Map<String ,Object>> getQuestionList(String where,String limit){
        String sql="select questionId as `id`,bornTime as `datetime`,isMust as `required`,type as `type`,choices as `options`,username as `creator`,questionBody as `title` from question left join user_table on creatorId=userid "+where+limit;

        List<Map<String,Object>> list= jdbcTemplate.queryForList(sql);
        for (Map<String,Object> m:list){
            m.put("options", JSON.parseArray(m.get("options").toString()));
        }
        return list;
    }
    public int getQuestionListTotalNum(String where){
        String sql="select count(*) from question "+where;
        return jdbcTemplate.queryForObject(sql,Integer.class);
    }

    public Question getQuestionById(int questionId) {
        try {
        return jdbcTemplate.queryForObject("select * from question where questionId=?", new RowMapper<Question>() {
            @Override
            public Question mapRow(ResultSet rs, int rowNum) throws SQLException {
                Question question=new Question();
                question.setQuestionId(rs.getInt("questionId"));
                question.setCreatorId(rs.getInt("creatorId"));
                question.setType(rs.getInt("type"));
                question.setQuestionBody(rs.getString("questionBody"));
                question.setMust(rs.getInt("isMust"));
                question.setChoices(JSON.parseArray(rs.getString("choices")));
                return question;
            }
        },questionId);}
        catch (Exception e){
            return null;
        }
    }

    public List<Map<String, Object>> getQuestionListStu(String where) {
        return jdbcTemplate.query("select questionId as `id`,questionBody as `title`,type as `type`,choices as `choices`,isMust as `required` from question " + where, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
                Map<String,Object> map=new HashMap<>();
                map.put("id",rs.getInt("id"));
                map.put("type",rs.getInt("type"));
                map.put("choices",JSON.parseArray(rs.getString("choices")));
                map.put("title",rs.getString("title"));
                map.put("required",rs.getInt("required"));
                return map;
            }
        });
    }

    public List<Map<String, Object>> getReporterQuestion(int id){
        return jdbcTemplate.queryForList("select * from question where creatorId = ?", id);
    }

    public void updateQuestion(Question question) {
        jdbcTemplate.update("update question set isMust=?,type=?,questionBody=?,choices=? where questionId=?",question.getIsMust(),question.getType(),question.getQuestionBody(),question.getChoices().toJSONString(),question.getQuestionId());
    }
    public int getQuestionCount(String studentId,int questionId){
       return jdbcTemplate.queryForObject("select count(*) from reporter_student_table where stuId=? and (exam like '%,"+questionId+",%' or exam like '%["+questionId+",%' or exam like '%,"+questionId+"]%' ) ",Integer.class,studentId);
    }
}
