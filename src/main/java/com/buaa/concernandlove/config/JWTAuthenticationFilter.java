package com.buaa.concernandlove.config;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.buaa.concernandlove.Service.UserService;
import com.buaa.concernandlove.pojo.User;
import com.buaa.concernandlove.utils.JwtTokenUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.crypto.Cipher;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.*;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;
    private UserService userService;
    public JWTAuthenticationFilter(AuthenticationManager authenticationManager,UserService userService){
        this.authenticationManager=authenticationManager;
        this.userService=userService;
    }
    @SneakyThrows
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        StringBuffer data = new StringBuffer();
        String line = null;
        BufferedReader reader = null;
        try {
            reader = request.getReader();
            while (null != (line = reader.readLine()))
                data.append(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = JSONObject.parseObject(data.toString());
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        System.out.println("username" + username);
        System.out.println("password" + password);

        if (!username.equals("admin")&&userService.getUserByStuId(username)==null){
            PrintWriter out = response.getWriter();
            Map<String, Object> map = new HashMap<>();
            map.put("code", 400);
            map.put("msg","User not found");
            out.write(new ObjectMapper().writeValueAsString(map));
            out.flush();
            out.close();
            return null;
        }
        System.out.println("Username is valid.");

        if(username.equals("admin")) {
            // 管理员要特判密钥
            JSONArray jsonArray = jsonObject.getJSONArray("private_key");
            String send_private_key = "";

            /*
            [----------------------------私钥请放置于此----------------------------------]
             */
            String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOpNwjYtoq1J9no1\n" +
                    "v+nrv7MWQBiPvoaPm9Tq9aas4d042WJP03PTW95aovkqUw7AQbd2UiNXPzX27c0W\n" +
                    "fQxTljpkRVqBoHAneKtJvmqrJm7ucWPEkyF8Hx64vkEEQamrauWalM3QITAHycCd\n" +
                    "kPfVgRPGGF//zdFbtcpZkVyzpfJZAgMBAAECgYEA2jrcpjuxJjgak09W1diXb4VM\n" +
                    "49OWwJwnp5PEg57IrJTTnIb/SwVUrvC6lNrAC6A0VV2mOXZ80JWSfg/xHrcH1sje\n" +
                    "5SUOvIW6hbd0Acl0sahqbpJx6GXBT/bDUnvVmfX+VLC3IiVL6rOntwEf8QRTappg\n" +
                    "Urynzlnc68Qppl14/2UCQQD42Y5KvnJvjkbNBtL1ptK+bcmaM7TkGXW1v6zYKvXV\n" +
                    "CG6FalA9dpphqUrHTar4XjS3sQb0I2olxHHAPL2jbzu7AkEA8Qk1F2zAlyCidNVy\n" +
                    "r/1n3j/W2yKllhCVEcKN/E2RZytkZPsarLkJaSu+Sp95Jno0l36KJdqyKDpBI03T\n" +
                    "M5AG+wJAMD300Nak1q2NWjflc8v4dQRRnfxCKCorYKWc+3cwmzD8RA79KT0x5pbs\n" +
                    "MUEEUQvqSmYeQ/wNmu6/YpYj6Wgp+wJBANPDjRt5dnLQPyp6ZPptnhXK7I82D1ia\n" +
                    "VP13epsR4w9LNjbpzfO11ajKv7J8DUpaz13R1LmRPDjPhW7o7wc8IIUCQG56tqms\n" +
                    "2bC1yCbgl2aI41FGOJaeDRKBbafONXdW2zW5V6PO+CktOAvVCUFN3oYse3irCFcF\n" +
                    "/aoMLZmQGc8r/QE=";


            //64位解码加密后的字符串
            byte[] inputByte = Base64.getDecoder().decode(privateKey.replace("\n", ""));
            RSAPrivateKey priKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(inputByte));
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, priKey);

            for(int i = 0; i < jsonArray.size(); i++) {
                String ciphertext = jsonArray.getString(i);
                byte[] cipherByte = Base64.getDecoder().decode(ciphertext);
                String outStr = new String(cipher.doFinal(cipherByte));
                send_private_key = send_private_key + outStr;
            }
            if(!send_private_key.equals(privateKey)) {
                PrintWriter out = response.getWriter();
                Map<String, Object> map = new HashMap<>();
                map.put("code", 400);
                map.put("msg","认证失败！");
                out.write(new ObjectMapper().writeValueAsString(map));
                out.flush();
                out.close();
                return null;
            }
        }

        User user= userService.getUserByStuId(username);
        List<SimpleGrantedAuthority> authorities=new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getUserRole()));
        PasswordEncoder passwordEncoder=new BCryptPasswordEncoder();
//        System.out.println(passwordEncoder.encode(password));
        if (!passwordEncoder.matches(password,user.getUserPassword())){
            PrintWriter out = response.getWriter();
            Map<String, Object> map = new HashMap<>();
            map.put("code", 400);
            map.put("msg","Password Error");
            System.out.println("Login Error");
            out.write(new ObjectMapper().writeValueAsString(map));
            out.flush();
            out.close();
            return null;
        }
        if (user.getBan()){
            PrintWriter out = response.getWriter();
            Map<String, Object> map = new HashMap<>();
            map.put("code", 400);
            map.put("msg","User banned!");
            System.out.println("Login Error");
            out.write(new ObjectMapper().writeValueAsString(map));
            out.flush();
            out.close();
            return null;
        }
        UsernamePasswordAuthenticationToken token=new UsernamePasswordAuthenticationToken(username, password,authorities);
        token.setDetails(user);
        return authenticationManager.authenticate(
                token
        );
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        UserDetails jwtUser= (UserDetails) authResult.getPrincipal();
        Collection<? extends GrantedAuthority> authorities = jwtUser.getAuthorities();
        String role="";
        for(GrantedAuthority authority : authorities){
            role = authority.getAuthority();
        }
        String token= JwtTokenUtil.createToken(jwtUser.getUsername(),role,userService.getUserByStuId(jwtUser.getUsername()).getUserid(),false);
        PrintWriter out = response.getWriter();
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);
        map.put("Authorization", token);  // 登录成功的对象
        map.put("type",role.equals("admin")?0:1);
        map.put("msg","登录成功");
        out.write(new ObjectMapper().writeValueAsString(map));
        out.flush();
        out.close();
        response.addHeader(JwtTokenUtil.TOKEN_HEADER, JwtTokenUtil.TOKEN_PREFIX + token);

    }
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        response.getWriter().write("authentication failed, reason: " + failed.getMessage());
    }
}
