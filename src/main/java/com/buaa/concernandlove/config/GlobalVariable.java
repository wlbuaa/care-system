package com.buaa.concernandlove.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "buaa.concernandlove")
public class GlobalVariable {
    private String localhost;
    private String publicKey;
    private String privateKey;
    private String AESKey;

    public String getAESKey() {
        return AESKey;
    }

    public void setAESKey(String AESKey) {
        this.AESKey = AESKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getLocalhost() {
        return localhost;
    }
    public void setLocalhost(String localhost) {
        this.localhost = localhost;
    }
}
