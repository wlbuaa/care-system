package com.buaa.concernandlove.config;

import com.buaa.concernandlove.Service.*;
import com.buaa.concernandlove.dao.UserDao;
import com.buaa.concernandlove.dao.WatchedStudentDao;
import com.buaa.concernandlove.pojo.User;
import com.buaa.concernandlove.utils.SendMessageUtil;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class ScheduleConfig {
    @Autowired
    private WatchedStudentService watchedStudentService;
    @Autowired
    UserService userService;
    @Autowired
    ReportService reportService;
    @Autowired
    ChartService chartService;
    @Autowired
    WarningService warningService;
    @Scheduled(cron = "0 0 0 * * ?")
    public void dailyUpdate(){
        userService.dailyUpdate();
        watchedStudentService.dailyUpdate();
    }
    @Scheduled(cron = "0 0 0 ? * MON")
    public void weeklyUpdate(){
        userService.weeklyUpdate();
        watchedStudentService.weeklyUpdate();
    }
    @Scheduled(cron = "0 0 0 1 * ?")
    public void monthlyUpdate(){
        userService.monthlyUpdate();
        watchedStudentService.monthlyUpdate();
    }
    //在整点后的15秒钟开启功能 防止误差
    @Scheduled(cron = "15 0/30 * * * *")
    public void autoRemind() {
        System.out.println("短信自动提醒功能开始");
        List<Map<String,Object>> reporterList=userService.getAllReporterInfo();
        Map<String,Object> chartMap=chartService.getChartManage();
        int timeInterval=(Integer)chartMap.get("interval");
        String chartMangerPhone=chartMap.get("phone").toString();
        String reporters="";
        for(Map<String,Object> reporterInfo:reporterList){
            int userId=0,studentId=0;
            Timestamp start=null;
            Timestamp end=null;
            try {
                userId = Integer.parseInt(reporterInfo.get("reId").toString());
                studentId = Integer.parseInt(reporterInfo.get("studentId").toString());
                start = (Timestamp) reporterInfo.get("startTime");
                end = (Timestamp) reporterInfo.get("endTime");
            }
            catch (Exception e){
                e.printStackTrace();
                continue;
            }
            Integer isStopped= (Integer) watchedStudentService.getWatchedStudentById(studentId).get(0).get("isStopped");
            System.out.println(studentId+":"+isStopped);
            if(isStopped==1){
                continue;
            }
            Timestamp now=new Timestamp(new Date().getTime());
            System.out.println(start);
            System.out.println(now);
            System.out.println(end);
            if(end.before(now)&&(now.getTime()-end.getTime())/1000/60<=30){
                if(reportService.getReportBetween(studentId,userId,start,now)==null){
                    User user=userService.getUserById(userId);
                    System.out.println("向上报员"+user.getUsername()+":"+user.getUserPhoneNumber()+"发送短信");
                    String phone=user.getUserPhoneNumber();
                    String url="http://39.106.1.86";
                    String desc=user.getUsername()+"同学，今日关心关爱的同学信息还未上报，请及时上报"+url;
                    try {
                        int code = SendMessageUtil.send(phone, desc);
                        System.out.println(SendMessageUtil.getMessage(code));
                    }catch (Exception e){
                        System.out.println("自动提醒上报异常");
                    }
                }
            }
            if(end.before(now)&&(now.getTime()-end.getTime())/1000/60>=timeInterval
            &&(now.getTime()-end.getTime())/1000/60<180){//超过3小时还不上报不再提醒辅导员了
                if(reportService.getReportBetween(studentId,userId,start,now)==null){
                    User user=userService.getUserById(userId);
                    String phone=user.getUserPhoneNumber();
                    String name=user.getUsername();
                    reporters = String.format("%s,(%s:%s)", reporters, name, phone);
                }
            }
            else {}
        }
        if(!reporters.equals("")) {
            reporters=reporters.substring(1,reporters.length());
            String desc = String.format("上报员%s未上报，请提醒他们上报！", reporters);
            try {
                int code = SendMessageUtil.send(chartMangerPhone, desc);
                System.out.println(SendMessageUtil.getMessage(code));
            } catch (Exception e) {
                System.out.println("告知辅导员未上报者列表异常。");
            }
        }
        else {
            System.out.println("没有需要提醒的上报员");
        }
        System.out.println("短信自动提醒功能结束");
    }

    @Scheduled(cron = "0 0/10 * * * ?")
    public void warning(){
        List<Map<String,Object>> unsolvedWarnings=warningService.getUnsolvedWarning();
        for(Map<String,Object> unsolvedWarning:unsolvedWarnings){
            Integer warningLevel=(Integer) unsolvedWarning.get("level");
            Integer studentId=(Integer)unsolvedWarning.get("studentId");
            Map<String,Object> stuInfo=watchedStudentService.getWatchedStudentById(studentId).get(0);
            String stuId=stuInfo.get("stuId").toString();
            String name=stuInfo.get("studentName").toString();
            List<User> reporters=userService.getReportersByWatchedStudentId(stuId);
            for (User reporter:reporters){
                String telephone=reporter.getUserPhoneNumber();
                String desc = name + "同学今日情况触发" + warningLevel + "级紧急预警，需及时确认情况。";
                try {
                    int code = SendMessageUtil.send(telephone, desc);
                    System.out.println(code);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
