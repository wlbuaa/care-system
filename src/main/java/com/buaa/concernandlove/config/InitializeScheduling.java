package com.buaa.concernandlove.config;

import com.buaa.concernandlove.Service.ChartService;
import com.buaa.concernandlove.Service.EmailService;
import com.buaa.concernandlove.Service.ReportService;
import com.buaa.concernandlove.Service.ScheduleService;
//import com.buaa.concernandlove.utils.CryptoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * 初始化类
 */
@Order(1) // @Order注解可以改变执行顺序，越小越先执行
@Component
public class InitializeScheduling implements ApplicationRunner {

    @Autowired
    ScheduleService scheduleService;
    @Autowired
    ChartService chartService;
    @Autowired
    GlobalVariable globalVariable;
    @Autowired
    EmailService emailService;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    ReportService reportService;

    /**
     * 会在服务启动完成后立即执行
     */
    @Override
    public void run(ApplicationArguments arg0) throws Exception {
        scheduleService.startScheduling();
        globalVariable.setAESKey("tepIL2mmIelZd5vK");

        /*
        [----------------------------公钥请放置于此----------------------------------]
        1024位，PKCS#8
         */
        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDqTcI2LaKtSfZ6Nb/p67+zFkAY\n" +
                "j76Gj5vU6vWmrOHdONliT9Nz01veWqL5KlMOwEG3dlIjVz819u3NFn0MU5Y6ZEVa\n" +
                "gaBwJ3irSb5qqyZu7nFjxJMhfB8euL5BBEGpq2rlmpTN0CEwB8nAnZD31YETxhhf\n" +
                "/83RW7XKWZFcs6XyWQIDAQAB";

        globalVariable.setPublicKey(publicKey);
//        scheduleService.SendEmailForEach(?, emailService, jdbcTemplate, chartService);
//        System.out.println(reportService.findQuestionAndAnswer(?));
    }

}