package com.buaa.concernandlove.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.buaa.concernandlove.dao.*;
import com.buaa.concernandlove.pojo.AnswerRecord;
import com.buaa.concernandlove.pojo.ReportRecord;
import com.buaa.concernandlove.pojo.User;
import com.buaa.concernandlove.utils.SendMessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {
    @Autowired
    private AnswerRecordDao answerRecordDao;
    @Autowired
    private ReportDao reportDao;
    @Autowired
    private WatchedStudentDao watchedStudentDao;
    @Autowired
    private WarningStuDao warningStuDao;
    @Autowired
    private UserDao userDao;
    public void addReport(JSONObject jsonObject,int userid){//记录一次填报,userid是填报者的id
        if (getTodayReportRecord(userid,(int)watchedStudentDao.getWatchedStudentById(jsonObject.getString("student_id")).get(0).get("studentId"))!=null){
            return;
        }
        ReportRecord reportRecord=new ReportRecord();
        reportRecord.setReporterId(userid);
        int studentId= (int) watchedStudentDao.getWatchedStudentById(jsonObject.getString("student_id")).get(0).get("studentId");
        reportRecord.setStudentId(studentId);
        reportRecord.setSpecialCondition(jsonObject.getString("special_situation"));
        int reportRecordId=reportDao.addReportRecord(reportRecord);
        JSONArray questions=jsonObject.getJSONArray("question_list");
        List<Map<String,Object>> ans = new ArrayList<>();
        for (int i = 0; i < questions.size(); i++) {
            AnswerRecord answerRecord=new AnswerRecord();
            JSONObject question=questions.getJSONObject(i);
            Map<String,Object> map = new HashMap<>();
            map.put("questionId",question.getInteger("questionId"));
            answerRecord.setQuestionId(question.getInteger("questionId"));
            answerRecord.setReportRecordId(reportRecordId);
            map.put("answer",question.getInteger("answer"));
            answerRecord.setAnswer(question.getInteger("answer"));
            answerRecordDao.addAnswerRecord(answerRecord);
            ans.add(map);
        }

        Map<String, Object> warnRecords;
        List<Map<String,Object>> reportRecords = reportDao.getStudentAnswerRecord(studentId);
        try {
            warnRecords = warningStuDao.getWarningStuDetail(studentId).get(0);
        } catch (IndexOutOfBoundsException e){
            return;
        }
        if((Boolean)warnRecords.get("is_ban"))return;
        JSONObject warnCondition1 = JSON.parseObject(warnRecords.get("warningCondition1").toString());
        JSONObject warnCondition2 = JSON.parseObject(warnRecords.get("warningCondition2").toString());
        JSONObject warnCondition3 = JSON.parseObject(warnRecords.get("warningCondition3").toString());
        JSONObject warnConditionEx1 = JSON.parseObject(warnRecords.get("warningConditionEx1").toString());
        JSONObject warnConditionEx2 = JSON.parseObject(warnRecords.get("warningConditionEx2").toString());
        JSONObject warnConditionEx3 = JSON.parseObject(warnRecords.get("warningConditionEx3").toString());
        JSONArray questionList1 = warnCondition1.getJSONArray("question");
        JSONArray questionList2 = warnCondition2.getJSONArray("question");
        JSONArray questionList3 = warnCondition3.getJSONArray("question");
        JSONArray questionExList1 = warnConditionEx1.getJSONArray("question");
        JSONArray questionExList2 = warnConditionEx2.getJSONArray("question");
        JSONArray questionExList3 = warnConditionEx3.getJSONArray("question");
        boolean relation1 = warnCondition1.getBoolean("insideRelation");
        boolean relation2 = warnCondition2.getBoolean("insideRelation");
        boolean relation3 = warnCondition3.getBoolean("insideRelation");
        boolean relationEx1 = warnConditionEx1.getBoolean("insideRelation");
        boolean relationEx2 = warnConditionEx2.getBoolean("insideRelation");
        boolean relationEx3 = warnConditionEx3.getBoolean("insideRelation");
        int warningLevel;
        boolean matching3 = isMatching(reportRecords, questionList3, relation3);
        System.out.println(matching3);
        System.out.println(questionExList3.size());
        if(questionExList3.size()!=0) matching3 &= isMatching(reportRecords, questionExList3, relationEx3);
        boolean matching2 = isMatching(reportRecords, questionList2, relation2);
        System.out.println(matching2);
        System.out.println(questionExList2.size());
        if(questionExList2.size()!=0) matching2 &= isMatching(reportRecords, questionExList2, relationEx2);
        boolean matching1 = isMatching(reportRecords, questionList1, relation1);
        System.out.println(matching1);
        System.out.println(questionExList1.size());
        if(questionExList1.size()!=0) matching1 &= isMatching(reportRecords, questionExList1, relationEx1);
        if(matching3)warningLevel=3;
        else if(matching2)warningLevel=2;
        else if (matching1)warningLevel=1;
        else return;
        System.out.println("warningLevel:"+warningLevel);
//        for(Map<String,Object> answer:reportRecords){
//            int questionId = (Integer) answer.get("questionId");
//            int userAns = Integer.parseInt(answer.get("answer").toString());
//            for(int i=0;i<questionList3.size();i++){
//                JSONObject question = questionList3.getJSONObject(i);
//                int warnAns = question.getInteger("ans");
//                int id = question.getInteger("id");
//                if(warnAns==userAns && id==questionId){
//                    warningLevel=3;
//                    break;
//                }
//            }
//            if(warningLevel!=0)break;
//            for(int i=0;i<questionList2.size();i++){
//                JSONObject question = questionList2.getJSONObject(i);
//                int warnAns = question.getInteger("ans");
//                int id = question.getInteger("id");
//                if(warnAns==userAns && id==questionId){
//                    warningLevel=2;
//                    break;
//                }
//            }
//            if(warningLevel!=0)break;
//            for(int i=0;i<questionList1.size();i++){
//                JSONObject question = questionList1.getJSONObject(i);
//                int warnAns = question.getInteger("ans");
//                int id = question.getInteger("id");
//                if(warnAns==userAns && id==questionId){
//                    warningLevel=1;
//                    break;
//                }
//            }
//            if(warningLevel!=0)break;
//        }

        if(warningStuDao.getWarningRecord(studentId,warningLevel)!=null){
            System.out.println("已触发更高级的预警");
        }
        else{
            warningStuDao.doWarning(studentId,warningLevel);
            List<String> tel = new ArrayList<>();
            JSONArray reporters;
            if(warningLevel==3)reporters = warnCondition3.getJSONArray("reporter");
            else if(warningLevel==2)reporters = warnCondition2.getJSONArray("reporter");
            else reporters = warnCondition1.getJSONArray("reporter");
            System.out.println("reporters:");
            System.out.println(reporters);
            for(int i=0;i<reporters.size();i++){
                JSONObject reporter = reporters.getJSONObject(i);
                int reporterId = reporter.getInteger("id");
                User user = userDao.getUserById(reporterId);
                if(user!=null&&!user.getBan())tel.add(user.getUserPhoneNumber());
            }
            System.out.println("tel:");
            System.out.println(tel);
            String name = watchedStudentDao.getWatchedStudentById(studentId).get(0).get("studentName").toString();
            System.out.println("name:");
            System.out.println(name);
            for (String telephone : tel) {
                String desc = name + "同学今日情况触发" + warningLevel + "级紧急预警，需及时确认情况。";
                try {
                    int code = SendMessageUtil.send(telephone, desc);
                    System.out.println(code);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isMatching(List<Map<String, Object>> reportRecords, JSONArray questionList,boolean relation) {
        if(questionList.size()==0)return false;
        if(relation){ //题目间为并
            for(int i=0;i<questionList.size();i++){
                JSONObject question = questionList.getJSONObject(i);
                int warnAns = question.getInteger("ans");
                int id = question.getInteger("id");
                int num = question.getInteger("num");
                System.out.println(question.toJSONString());
                System.out.println("------");
                boolean matching = false;
                for(Map<String,Object> answer:reportRecords){
                    int questionId = (Integer) answer.get("questionId");
                    int userAns = Integer.parseInt(answer.get("answer").toString());
                    Long times= (Long)answer.get("num");
                    System.out.println(answer.toString());
                    if(questionId==id && userAns==warnAns && times.intValue()>=num){
                        matching = true;
                        break;
                    }
                }
                if(!matching){
                    return false; // 有一个不匹配返回假
                }
            }
            return true; // 都匹配返回真
        } else { //题目间为或
            for(int i=0;i<questionList.size();i++){
                JSONObject question = questionList.getJSONObject(i);
                int warnAns = question.getInteger("ans");
                int id = question.getInteger("id");
                int num = question.getInteger("num");
                System.out.println(question.toJSONString());
                System.out.println("------");
                for(Map<String,Object> answer:reportRecords){
                    int questionId = (Integer) answer.get("questionId");
                    int userAns = Integer.parseInt(answer.get("answer").toString());
                    Long times = (Long) answer.get("num");
                    System.out.println(answer.toString());
                    if(questionId==id && userAns==warnAns && times.intValue()>=num){
                        return true; // 有一个匹配则返回真
                    }
                }
            }
            return false; // 都不匹配返回假
        }
    }

    public ReportRecord getReportBetween(int studentId, int reporterId, Timestamp start, Timestamp end){
        return reportDao.findReportBetween(studentId,reporterId,start,end);
    }
    public ReportRecord getTodayReportRecord(int reporterId,int studentId){
        return reportDao.findTodayReportRecord(reporterId,studentId);
    }

    public String findQuestionAndAnswer(int reportId) {
        return reportDao.findQuestionAndAnswer(reportId);
    }
}
