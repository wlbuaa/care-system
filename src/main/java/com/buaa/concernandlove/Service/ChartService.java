package com.buaa.concernandlove.Service;

import com.buaa.concernandlove.dao.*;
import com.buaa.concernandlove.pojo.Question;
import com.buaa.concernandlove.pojo.ReportRecord;
import com.buaa.concernandlove.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChartService {
    @Autowired
    ChartDao chartDao;
    @Autowired
    ReportDao reportDao;
    @Autowired
    AnswerRecordDao answerRecordDao;
    @Autowired
    QuestionDao questionDao;
    @Autowired
    ConvRecordDao convRecordDao;
    @Autowired
    UserDao userDao;
    @Autowired
    ScheduleService scheduleService;

    public Map<String, Object> getChartManage() {
        return chartDao.getChartManage();
    }

    public List<ReportRecord> getReportsBetween(int studentId, String start, String end){
        return reportDao.findReportsBetween(studentId,start,end);
    }

    public int setChartManage(int level1_count, int level2_count, List<String> level1_time, List<String> level2_time,
                              int range_time, String mail,
                              int catch_count, List<String> catch_time, String phone, int time_interval) {
        int result = chartDao.setChartManage(level1_count, level2_count, level1_time, level2_time, range_time, mail,
                catch_count, catch_time, phone, time_interval);
        // 设置修改后，需要重启
        scheduleService.cancelScheduling();
        scheduleService.startScheduling();
        return result;
    }

    public int isReportOnTime(int reporterId,int studentId){
        Map<String, Object> reporter=new HashMap<>();
        try {
            reporter = userDao.getReporterByStudentIdAndReporterId(reporterId, studentId).get(0);
        }
        catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        Timestamp end=(Timestamp)reporter.get("endTime");
        Timestamp start=(Timestamp)reporter.get("startTime");
        if(reportDao.findReportBetween(studentId,reporterId,start,end)==null){
            return 0;
        }
        else {
            return 1;
        }
    }

    public int isReportOnTimeByDay(int reporterId,int studentId, Date day){
        Map<String, Object> reporter=new HashMap<>();
        try {
            reporter = userDao.getReporterByStudentIdAndReporterId(reporterId, studentId).get(0);
        }
        catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        Timestamp end=(Timestamp)reporter.get("endTime");
        Timestamp start=(Timestamp)reporter.get("startTime");

        // 将start和end的日期同步到制定的日期
        start.setYear(day.getYear());
        start.setMonth(day.getMonth());
        start.setDate(day.getDate());

        end.setYear(day.getYear());
        end.setMonth(day.getMonth());
        end.setDate(day.getDate());


        if(reportDao.findReportBetween(studentId,reporterId,start,end)==null){
            return 0;
        }
        else {
            return 1;
        }
    }

    public int countScoresByReportRecord(int reportRecordId){
        List<Map<String,Object>> answerList=answerRecordDao.getAllAnswerByReportId(reportRecordId);
        System.out.println(answerList);
        int count=0;
        int questionNum=0;
        for(Map<String,Object> answer:answerList){
            int questionId=Integer.parseInt(answer.get("questionId").toString());
            Question question=questionDao.getQuestionById(questionId);
            int ans=Integer.parseInt(answer.get("answer").toString());
            if(question.getType()==1){
                //选择
                switch (ans){
                    case 0: {
                        count+=10;
                        break;
                    }
                    case 1:{
                        count+=6;
                        break;
                    }
                    case 2:{
                        count+=3;
                        break;
                    }
                    default:{
                        count+=0;
                    }
                }
            }
            else {
                //判断
                switch (ans){
                    case 0:{
                        count+=10;
                        break;
                    }
                    default:{
                        count+=0;
                    }
                }
            }
        }
        return count;
    }

    public int countMovesBetween(String watchedStudentId,String startStr,String endStr){
        return convRecordDao.countMoveTimesBetween(watchedStudentId,startStr,endStr);
    }

    public Timestamp getClockTime(String watchedStudentId,String startStr,String endStr){
        return convRecordDao.getClockTime(watchedStudentId,startStr,endStr);
    }
}
