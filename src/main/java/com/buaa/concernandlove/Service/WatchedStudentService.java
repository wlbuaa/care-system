package com.buaa.concernandlove.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.buaa.concernandlove.dao.QuestionDao;
import com.buaa.concernandlove.dao.ReportDao;
import com.buaa.concernandlove.dao.UserDao;
import com.buaa.concernandlove.dao.WatchedStudentDao;
import com.buaa.concernandlove.pojo.Question;
import com.buaa.concernandlove.pojo.WatchedStudentDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

@Service
public class WatchedStudentService {
    @Autowired
    WatchedStudentDao watchedStudentDao;
    @Autowired
    ReportDao reportDao;
    @Autowired
    UserDao userDao;
    @Autowired
    QuestionDao questionDao;
    public int addWatchedStudent(WatchedStudentDetail watchedStudent){
        return watchedStudentDao.addWatchedStudent(watchedStudent);
    }
    public List<Map<String,Object>> getWatchedStudentById(int id){
        return watchedStudentDao.getWatchedStudentById(id);
    }
    public List<Map<String,Object>> getWatchedStudentById(String stuId){
        return watchedStudentDao.getWatchedStudentById(stuId);
    }
    public List<Map<String,Object>> getAllStudent(){
        return watchedStudentDao.getAllStudent();
    }
    public List<Map<String,Object>> getAllStudentByPage(int pageNum, int pageSize) {
        return watchedStudentDao.getAllStudentByPage(pageNum,pageSize);
    }
    public int getAllStudentPageNum() {
        return watchedStudentDao.getAllStudentPageNum();
    }
    public List<Map<String,Object>> getKeyStudentByPage(int pageNum, int pageSize, String name) {
        return watchedStudentDao.getKeyStudentByPage(pageNum,pageSize,name);
    }
    public int getKeyStudentPageNum(String name) {
        return watchedStudentDao.getKeyStudentPageNum(name);
    }
    public int updateStuInfo(WatchedStudentDetail watchedStudentDetail){
        return watchedStudentDao.updateStuInfo(watchedStudentDetail);
    }
    public int deleteStudent(int id){
        return watchedStudentDao.deleteStudent(id);
    }
    public List<Map<String,Object>> getCouriersById(int id){
        return watchedStudentDao.getCouriersById(id);
    }
    public int trackStudent(int studentId) {
        return watchedStudentDao.trackStudent(studentId);
    }
    public int untrackStudent(int studentId) {
        return watchedStudentDao.untrackStudent(studentId);
    }

    public void setReported0() {
        watchedStudentDao.setReported0();
    }

    public void updateWatchedStudentReported(int watchedStudentId){
        if(userDao.countReportersByWatchedStudentId(watchedStudentId)>
                reportDao.countReportedReportersByWatchedStudentId(watchedStudentId)){
            watchedStudentDao.setReported0(watchedStudentId);
        }
        else {
            watchedStudentDao.setReported1(watchedStudentId);
        }
    }

    public List<Map<String, Object>> getReportMessage(int studentId,int reporterId, long startTime, Long endTime, int sort, int pagenum, int pagesize) {
        String where=" where studentId="+studentId+" and reporterId="+reporterId+" reportTime between "+startTime+" and "+endTime;
        String orderBy=" order by reportTime "+(sort==0?"desc":"");

        return watchedStudentDao.getReportMessage(where,orderBy);
    }
    public List<Map<String, Object>> getReportMessage(int studentId) {
        String where=" where studentId="+studentId+" ";
        String orderBy="";
        return watchedStudentDao.getReportMessage(where,orderBy);
    }
    public int getReportMessageTotalNum(int studentId,int reporterId, long startTime, Long endTime) {
        String where=" where studentId="+studentId+" and reporterId="+reporterId+" reportTime between "+startTime+" and "+endTime;
        return watchedStudentDao.getReportMessageTotalNum(where);
    }
    public int getReportMessageTotalNum(int studentId) {
        String where=" where studentId="+studentId+" ";
        return watchedStudentDao.getReportMessageTotalNum(where);
    }
    public List<Map<String, Object>> getWatchedStudentList(String userStuId) {
        return watchedStudentDao.getWatchedStudentList(userStuId);
    }

    public Map<String,Object> getPaperByReporterAndStudent(String reporterId, String studentId) {
        Map<String,Object> reportRecord=watchedStudentDao.getWatchStudentByBoth(reporterId,studentId).get(0);
        JSONArray questionIds= JSON.parseArray(reportRecord.get("exam").toString());
        Map<String,Object> result=new HashMap<>();
        List<Question> questions=new ArrayList<>();
        for (int i = 0; i < questionIds.size(); i++) {
            int questionId=questionIds.getIntValue(i);
            Question question=questionDao.getQuestionById(questionId);
            if (question!=null){
            questions.add(question);}

        }
        result.put("paper",questions);
        switch (reportRecord.get("frequency").toString()){
            case "每天一次":{
                result.put("isReported",reportDao.findTodayReportRecord(reporterId,studentId)==null?0:1);
                break;
            }
            case "每周一次":{
                result.put("isReported",reportDao.findThisWeekReportRecord(reporterId,studentId)==null?0:1);
                break;
            }
            case "每月一次":{
                result.put("isReported",reportDao.findThisMonthReportRecord(reporterId,studentId)==null?0:1);
                break;
            }
            default:{
                result.put("isReported",0);
            }
        }
        return result;
    }

    public List<Map<String, Object>> getCouriersByIdNewName(int id) {
        return watchedStudentDao.getCouriesByIdNewName(id);
    }

    public void dailyUpdate(){
        watchedStudentDao.dailyUpdateIsReportedOfWatchedStudent();
    }

    public void weeklyUpdate(){
        watchedStudentDao.weeklyUpdateIsReportedOfWatchedStudent();
    }

    public void monthlyUpdate(){
        watchedStudentDao.monthlyUpdateIsReportedOfWatchedStudent();
    }
    public Map<String,Object> getQAndAById(int id){
        return watchedStudentDao.getQAndAById(id);
    }

    public Timestamp getStartTime(String studentStuId,int reporterId){
        return watchedStudentDao.getStartTime(studentStuId,reporterId);
    }

    public Timestamp getEndTime(String studentStuId, int userid) {
        return watchedStudentDao.getEndTIme(studentStuId,userid);
    }

    public List<Map<String, Object>> getConvRecord(String id) {
        return watchedStudentDao.getConvRecord(id);
    }

    // 按天来获取某个学号的打卡记录
    public List<Map<String, Object>> getConvRecordByDay(String id, Calendar day) {
        return watchedStudentDao.getConvRecordByDay(id, day);
    }
}
