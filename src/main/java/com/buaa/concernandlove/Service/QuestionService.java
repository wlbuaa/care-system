package com.buaa.concernandlove.Service;

import com.alibaba.fastjson.JSONArray;
import com.buaa.concernandlove.dao.QuestionDao;
import com.buaa.concernandlove.pojo.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Service
public class QuestionService {
    @Autowired
    private QuestionDao questionDao;
    public int addQuestion(Question question){
        return questionDao.addQuestion(question);
    }
    public int deleteQuestions(JSONArray jsonArray){
        return questionDao.deleteQuestionById(jsonArray);
    }
    public List<Map<String,Object>> getQuestionList(int pageSize, int pageNum, String keyWord, int isMustFilter, int typeFilter, Timestamp startTime,Timestamp endTime){
        String where=" where bornTime between \'"+startTime+"\' and \'"+endTime+"\' ";
        String limit=" limit "+(pageNum-1)*pageSize+","+pageSize;
        if (isMustFilter==0){
            where+=" and isMust=0 ";
        }else if (isMustFilter==1){
            where+=" and isMust=1 ";
        }
        if (typeFilter==1){
            where+=" and type=1 ";
        }else if(typeFilter==2){
            where+=" and type=2";
        }
        if (keyWord!=""){
            where+=" and questionBody like \'%"+keyWord+"%\' ";
        }

        return questionDao.getQuestionList(where,limit);
    }
    public int getQuestionListPageNum(String keyWord,int isMustFilter,int typeFilter,Timestamp startTime,Timestamp endTime){
        String where=" where bornTime between \'"+startTime+"\' and \'"+endTime+"\' ";
        if (isMustFilter==0){
            where+=" and isMust=0 ";
        }else if (isMustFilter==1){
            where+=" and isMust=1 ";
        }
        if (typeFilter==1){
            where+=" and type=1 ";
        }else if(typeFilter==2){
            where+=" and type=2";
        }
        if (keyWord!=""){
            where+=" and questionBody like \'%"+keyWord+"%\' ";
        }
        return questionDao.getQuestionListTotalNum(where);
    }

    public List<Map<String, Object>> getQuestionListStu(int type, int required, String keyWord) {
        String where=" where questionBody like '%"+keyWord+"%' ";
        switch (type){
            case 1:{
                where+=" and type= 1";
                break;
            }
            case 2:{
                where+=" and type =2 ";
                break;
            }
        }
        switch (required){
            case 1:{
                where+=" and isMust=1 ";
                break;
            }
            case 0:{
                where+=" and isMust=0 ";
                break;
            }
        }
        return questionDao.getQuestionListStu(where);
    }

    public List<Map<String, Object>> getReporterQuestion(int id){
        return questionDao.getReporterQuestion(id);
    }

    public void updateQuestion(Question question) {
        questionDao.updateQuestion(question);
    }
    public int getQuestionCount(String studentId,int questionId){
        return questionDao.getQuestionCount(studentId,questionId);
    }
}
