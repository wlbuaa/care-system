package com.buaa.concernandlove.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.buaa.concernandlove.pojo.ReportRecord;
import com.buaa.concernandlove.pojo.User;
import com.buaa.concernandlove.utils.CalendarUtil;
import com.buaa.concernandlove.utils.JsonUtil;
import com.buaa.concernandlove.utils.SendMessageUtil;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfDocumentInfo;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.File;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ScheduledFuture;

@Service
public class ScheduleService {
    private ConcurrentTaskScheduler taskScheduler = new ConcurrentTaskScheduler();
    private ScheduledFuture future1 = null, future2 = null, future3 = null, future4 = null;

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    EmailService emailService;
    @Autowired
    ChartService chartService;
    @Autowired
    ReportService reportService;
    @Autowired
    UserService userService;
    @Autowired
    WatchedStudentService watchedStudentService;

//    private String producePersonalReport(int studentId, int range_time, ChartService chartService) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//
//        String Ans = "--------最近"+range_time+"天信息报表-------\n";
//
//        Calendar startCalendar = Calendar.getInstance();
//        // 获取range_time天之前的日期
//        startCalendar.add(Calendar.DATE, -range_time+1);
////        System.out.println("Start Date = " + startCalendar.getTime());
//
//        for(int i = 1; i <= range_time; i++) {
//            Calendar nowCalendar = (Calendar) startCalendar.clone();
//            nowCalendar.add(Calendar.DATE, i);
//
//            // 设置时分秒毫秒为0
//            nowCalendar.set(Calendar.HOUR_OF_DAY, 0); //时
//            nowCalendar.set(Calendar.MINUTE, 0); // 分钟
//            nowCalendar.set(Calendar.SECOND, 0); //秒
//            nowCalendar.set(Calendar.MILLISECOND, 0); //毫秒
//
//            // 获取开始和结束的字符串
//            String endStr = sdf.format(nowCalendar.getTime());
//            nowCalendar.add(Calendar.DATE, -1);
//            String startStr = sdf.format(nowCalendar.getTime());
//
////            System.out.println("Current Day = " + startStr);
//
//            // 产生每天的平均分
//            List<ReportRecord> reportRecords=chartService.getReportsBetween(studentId,startStr,endStr);
//            double ave=0;
//            int num=0;
//            for(ReportRecord reportRecord:reportRecords){
//                ave+=chartService.countScoresByReportRecord(reportRecord.getRecordId());
//                num++;
//            }
//            if(num>0) ave /= num;
//            Ans = String.format("%s%s:  %f分\n", Ans, dateFormat.format(nowCalendar.getTime()), ave);
//        }
//        return Ans;
//    }

    // 发pdf文件，把所有学生的情况汇总成一个发送
    public void SendEmailForEach(int level, EmailService emailService, JdbcTemplate jdbcTemplate, ChartService chartService) {
        System.out.println("Sending Email...");

        // 获取数据库里的邮件地址
        Map<String, Object> map = jdbcTemplate.queryForMap("select mail, range_time from chart_manage where id=1");
//        String mailto = (String) map.get("mail");
        String[] mailtoArray=null;
        try {
            mailtoArray=map.get("mail").toString().split(",");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        // 获取等级等于level的学生列表
        List<Map<String, Object>> studentList = jdbcTemplate.queryForList("select studentName,studentId from watch_student_detail where concernLevel=?", level);
        System.out.println("级别"+level+": 含有"+studentList.size()+"名学生信息的报表邮件需要发送。");

        try {
            // 建立文档
            long timestamp = System.currentTimeMillis();
            String Pdf_name = timestamp + ".pdf";

            PdfWriter pdfWriter = new PdfWriter(new File(Pdf_name));
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);

            Document document = new Document(pdfDocument);
            PdfDocumentInfo documentInfo = pdfDocument.getDocumentInfo();

            documentInfo.setCreator("北航关心关爱系统");

            // 开始遍历学生、写入文档
            for (Map<String, Object> studentMap : studentList) {
                try {
                    // 准备好一些单个学生的信息
                    String stuName = studentMap.get("studentName").toString();
                    String studenId = studentMap.get("studentId").toString();
                    int range_time = (int) map.get("range_time");

                    producePersonalReport(document, level, Integer.parseInt(studenId), range_time, chartService, stuName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // 关闭文件
            document.close();
            String content = "这是所有级别"+level+"的报表，共"+studentList.size()+"人，请查收！";
            File PdfFile = new File(Pdf_name);
            for(String mailto:mailtoArray){
                emailService.SendEmail(mailto, "关心关爱系统每日推送报告", content, PdfFile);
            }
            // 发送完即刻删除，不留垃圾
            PdfFile.delete();
            System.out.println("[End] Sending Email Complete.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 追加pdf文件
    public void producePersonalReport(Document document, int level, int student_no, int range_time, ChartService chartService, String stuName) {
        // 生成相关的日期
        String studentId = (String) watchedStudentService.getWatchedStudentById(student_no).get(0).get("stuId");

        Calendar end = Calendar.getInstance();
        Calendar start = Calendar.getInstance();
        start.add(Calendar.DATE, -range_time+1);

        // 设置时分秒毫秒为0
        start.set(Calendar.HOUR_OF_DAY, 0); //时
        start.set(Calendar.MINUTE, 0); // 分钟
        start.set(Calendar.SECOND, 0); //秒
        start.set(Calendar.MILLISECOND, 0); //毫秒

        // 先备份这个start
        Calendar start_copy = Calendar.getInstance();
        start_copy.setTime(start.getTime());

        // end不需要备份

        // 设置日期格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat Hm = new SimpleDateFormat("HH:mm");

        // 年月日格式的
        SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");

        // 准备要写入的统计数据
        List<String> clockTime = new ArrayList<>();
        List<Double> score = new ArrayList<>();
        List<Integer> onTime = new ArrayList<>();
        List<String> backTime = new ArrayList<>();
        List<Integer> posChange = new ArrayList<>();
        List<String> date = new ArrayList<>();
        List<String> updateLocation = new ArrayList<>();

        Calendar tmp = Calendar.getInstance();
        tmp.setTime(start.getTime());
        int stuId = Integer.parseInt(watchedStudentService.getWatchedStudentById(studentId).get(0).get("studentId").toString());

        // 循环：从开始日期(range_time天之前）到今天
        while(start.before(end)){
            // 向日期列表中加入当前的日期
            date.add(day.format(start.getTime()));

            // tmp始终是start的后一天
            tmp.add(Calendar.DATE,1);

            // 从上报记录中计算分数 和 上报员的上报次数
            List<ReportRecord> reportRecords=chartService.getReportsBetween(stuId,
                    sdf.format(start.getTime()),sdf.format(tmp.getTime()));
            double ave=0;
            int num=0;
            int onTimeRecordNum=0;
            for(ReportRecord reportRecord:reportRecords){
                ave+=chartService.countScoresByReportRecord(reportRecord.getRecordId());
                onTimeRecordNum+=chartService.isReportOnTimeByDay(reportRecord.getReporterId(),reportRecord.getStudentId(),start.getTime());
                num++;
            }
            if(num>0) ave /= num;
            score.add(ave);

            // 记录一周内及时打卡的次数
            onTime.add(onTimeRecordNum);

            // 获取近一周位置移动次数
            Calendar oneWeekBefore=Calendar.getInstance();
            oneWeekBefore.setTime(start.getTime());
            oneWeekBefore.add(Calendar.DATE,-7);
            int moveTimes=chartService.countMovesBetween(studentId, sdf.format(oneWeekBefore.getTime()),
                    sdf.format(tmp.getTime()));

            posChange.add(moveTimes);

            // 每天的打卡位置
            // 如果没打卡的话，列表应该会为空，触发下标越界异常，就直接填入空数据即可
            try {
                String location = (String) watchedStudentService.getConvRecordByDay(studentId, start).get(0).get("report_location");
                updateLocation.add(location);
            }
            catch (Exception e) {
                updateLocation.add("");
            }

            // 记录打卡时间
//            System.out.println("["+sdf.format(start.getTime())+","+sdf.format(tmp.getTime())+"]");
            Timestamp clockTimeStamp=chartService.getClockTime(studentId,sdf.format(start.getTime()),sdf.format(tmp.getTime()));
            if (clockTimeStamp!=null)clockTime.add(Hm.format(clockTimeStamp));
            else clockTime.add("");

            // start前进一天
            start.add(Calendar.DATE,1);
        }

        // 以下是向pdf中追加内容
        try {
            // 写入一段文字描述
            Paragraph paragraph = new Paragraph(String.format("级别%d %s 最近%d天情况报表：", level, stuName, range_time));

            // 设置中文字体
            PdfFont font = PdfFontFactory.createFont("STSongStd-Light", "UniGB-UCS2-H", true);
            paragraph.setFont(font);
//        paragraph.setBackgroundColor(Color.LIGHT_GRAY);

            // 添加段落
            document.add(paragraph);

            // 创建信息表格
            Table table = new Table(new float[]{16, 16, 16, 16, 16, 16});
            table.setWidthPercent(100);

            // 表头的设置
            ArrayList<String> headers = new ArrayList<>();
            headers.add("日期");
            headers.add("打卡时间");
            headers.add("上报平均分");
            headers.add("当天上报员准时上报次数");
            headers.add("最近一周位置变动次数");
            headers.add("打卡位置");

            // 添加表头
            for (String header : headers) {
                Cell cell = new Cell();
                Paragraph paragraph1 = new Paragraph(header);
                paragraph1.setFont(font);
//            paragraph1.setFontSize(6);
                cell.add(paragraph1);
                table.addCell(cell);
            }

            // 添加单元格
            for (int i = 0; i < date.size(); i++) {
                add_cell(date.get(i), table, font);
                add_cell(clockTime.get(i), table, font);
                add_cell(String.format("%.2f", score.get(i)), table, font);
                add_cell(onTime.get(i)+"", table, font);
                add_cell(posChange.get(i)+"", table, font);
                add_cell(updateLocation.get(i), table, font);
            }

            // 向文档中嵌入表格
            document.add(table);

            // 插入表格后的换行
            paragraph = new Paragraph("\n");
            paragraph.setFont(font);
            document.add(paragraph);

            // 每个上报员的问题及答案
            // 遍历最近range_time天的答卷
            start.setTime(start_copy.getTime());
            tmp.setTime(start_copy.getTime());
            while(start.before(end)) {
                tmp.add(Calendar.DATE, 1);
                String date_str = day.format(start.getTime());

                // 插入日期
                paragraph = new Paragraph(date_str);
                paragraph.setFont(font);
                document.add(paragraph);

                // 获取回答记录
                List<ReportRecord> reportRecords=chartService.getReportsBetween(stuId,
                        sdf.format(start.getTime()),sdf.format(tmp.getTime()));
                for(ReportRecord reportRecord : reportRecords) {
                    String toOutput = "";
                    // 写入上报员的姓名
                    toOutput += String.format("上报员 %s:\n", userService.getReporterNameById(reportRecord.getReporterId()));

                    // 写上上报的时分
                    String upTimeHm = Hm.format(new Date(reportRecord.getReportTime().getTime()));
                    toOutput += String.format("上报时间: %s\n", upTimeHm);

                    // 写入问卷
                    toOutput += reportService.findQuestionAndAnswer(reportRecord.getRecordId());

                    // 上报的特殊情况
                    toOutput += String.format("特殊情况：%s\n", reportRecord.getSpecialCondition());

                    paragraph = new Paragraph(toOutput);
                    paragraph.setFont(font);
                    document.add(paragraph);
                }

                // 每个日期之后的换行
                paragraph = new Paragraph("\n");
                paragraph.setFont(font);
                document.add(paragraph);

                start.add(Calendar.DATE, 1);
            }

            // 整体换行
            paragraph = new Paragraph("\n\n\n");
            paragraph.setFont(font);
            document.add(paragraph);

            System.out.println("Append PDF OK!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 添加一个小单元格
    private void add_cell(String text, Table table, PdfFont font) {
        Cell cell1 = new Cell();
        Paragraph paragraph1 = new Paragraph(text);
        paragraph1.setFont(font);
//            paragraph1.setFontSize(6);
        cell1.add(paragraph1);
        table.addCell(cell1);
    }

    // 等级1 和 等级2 不在一个时间发送
    public void startScheduling() {
        System.out.println("Start Scheduling...");

        // 定时发送邮件——等级1
        future1 = taskScheduler.schedule(new Runnable() {
            @Override
            public void run() {
                try {
                    SendEmailForEach(1, emailService, jdbcTemplate, chartService);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                System.out.println("Trigger Application is started.");
                // 从数据库获取推送报表的时间
                Map<String, Object> map = jdbcTemplate.queryForMap("select level1_time,level2_time from chart_manage where id=1");
                String level1_time = (String) map.get("level1_time");
                // String(json)转List
                List<String> timeList = JsonUtil.jsonToList(level1_time);

                return CalendarUtil.findNextExecTime(timeList);
            }
        });

        // 定时发送邮件————等级2
        future2 = taskScheduler.schedule(new Runnable() {
            @Override
            public void run() {
                try {
                    SendEmailForEach(2, emailService, jdbcTemplate, chartService);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                System.out.println("Trigger Application is started.");
                // 从数据库获取推送报表的时间
                Map<String, Object> map = jdbcTemplate.queryForMap("select level1_time,level2_time from chart_manage where id=1");
                String level2_time = (String) map.get("level2_time");
                // String(json)转List
                List<String> timeList = JsonUtil.jsonToList(level2_time);

                //以下为找到下次执行任务的时间
                return CalendarUtil.findNextExecTime(timeList);
            }
        });

        // 定时爬取学校的接口
        future3 = taskScheduler.schedule(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("[Working] 正在抓取学校接口的数据！");
                    Runtime runtime = Runtime.getRuntime();
                    System.out.println("数据爬取开始");
                    Process p = runtime.exec("python /www/care/crawl.py ");
                    System.out.println("数据爬取结束");
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Trigger() {
            @Override
            public Date nextExecutionTime(TriggerContext triggerContext) {
                System.out.println("Trigger Application is started.");
                // 从数据库获取推送报表的时间
                Map<String, Object> map = jdbcTemplate.queryForMap("select catch_time from chart_manage where id=1");
                String catch_time = (String) map.get("catch_time");
                // String(json)转List
                List<String> timeList = JsonUtil.jsonToList(catch_time);

                //以下为找到下次执行任务的时间
                return CalendarUtil.findNextExecTime(timeList);
            }
        });

//        // 定时获取发送信息给辅导员
//        // 当前默认上报员最晚上报时间是22点
//        int hour = 22;
//        int minute = 0;
//        int time_interval = (int) jdbcTemplate.queryForMap("select time_interval from chart_manage where id=1").get("time_interval");
//
//        minute = minute + time_interval;
//        hour += minute / 60;
//        minute %= 60;
//
//        future4 = taskScheduler.schedule(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("检查各上报员是否按时打卡，如果没有，就短信告知辅导员。");
//
//                List<Map<String,Object>> reporterList = userService.getAllReporterInfo();
//                String reporters = "";
//                for(Map<String,Object> reporterInfo:reporterList){
//                    int userId=Integer.parseInt(reporterInfo.get("reId").toString());
//                    int studentId=Integer.parseInt(reporterInfo.get("studentId").toString());
//                    if(reportService.getTodayReportRecord(userId,studentId)==null){
//                        User user=userService.getUserById(userId);
//                        String phone=user.getUserPhoneNumber();
//                        String name=user.getUsername();
//                        reporters = String.format("%s, %s(电话：%s)", reporters, name, phone);
//                    }
//                }
//
//                if(!reporters.equals("")) {
//                    String desc = String.format("上报员 %s 未上报，请提醒他们上报！", reporters);
//                    String phone = (String) jdbcTemplate.queryForMap("select phone from chart_manage where id=1").get("phone");
//                    try {
//                        int code = SendMessageUtil.send(phone, desc);
//                        System.out.println(SendMessageUtil.getMessage(code));
//                    } catch (Exception e) {
//                        System.out.println("告知辅导员未上报者列表异常。");
//                    }
//                }
//            }
//        }, new CronTrigger(String.format("0 %d %d * * ?", minute, hour)));
    }

    public void cancelScheduling() {
        if(future1 != null) future1.cancel(true);
        if(future2 != null) future2.cancel(true);
        if(future3 != null) future3.cancel(true);
//        if(future4 != null) future4.cancel(true);
        System.out.println("Current Scheduling has been cancelled.");
    }
}
