package com.buaa.concernandlove.Service;

import com.alibaba.fastjson.JSONObject;
import com.buaa.concernandlove.dao.WarningStuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class WarningService {

    @Autowired
    WarningStuDao warningStuDao;

    public int addWarningStu(int id){
        return warningStuDao.insert(id);
    }

    public int deleteWarningStu(int id){
        return warningStuDao.delete(id);
    }

    public int changeWarningStu(int id){
        return warningStuDao.changeStatus(id);
    }

    public Page<Map<String, Object>> getWarningStu(int pageNum, int pageSize){
        PageRequest pagable = PageRequest.of(pageNum, pageSize);
        return warningStuDao.getWarningStuList(pagable);
    }

    public Page<Map<String, Object>> getWarningStuLog(int studentId, int pageNum, int pageSize){
        PageRequest pagable = PageRequest.of(pageNum, pageSize);
        return warningStuDao.getWarningStuLog(studentId,pagable);
    }

    public int updateStuWarning(int id, String warn1, String warn2, String warn3,String warn1ex,String warn2ex,String warn3ex){
        return warningStuDao.updateWarning(id, warn1, warn2, warn3,warn1ex,warn2ex,warn3ex);
    }

    public Map<String, Object> getStuWarningDetail(int id){
        return warningStuDao.getWarningStuDetail(id).get(0);
    }

    public JSONObject getReporterQuestion(int studentId){
        return warningStuDao.getReportersQuestions(studentId);
    }

    public void solve(int studentid, int logid){
        warningStuDao.solveLog(studentid, logid);
    }

    public List<Map<String,Object>> getUnsolvedWarning(){
        return warningStuDao.getUnsolvedWarning();
    }

}
