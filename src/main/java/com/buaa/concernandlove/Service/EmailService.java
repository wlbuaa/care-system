package com.buaa.concernandlove.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

@Service
public class EmailService {
    @Autowired
    private JavaMailSender mailSender;
    public void SendEmail(String to,String subject,String content,File file) throws MessagingException {
        MimeMessage mineMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mineMessage,true);
        String from = "se_email_sender@163.com";
        helper.setFrom(from);
        helper.setTo(to.split(","));
        helper.setSubject(subject);
        helper.setText(content);
        if(file != null) {
            FileSystemResource attachedFile = new FileSystemResource(file);
            helper.addAttachment("test.pdf",attachedFile);
        }
        mailSender.send(mineMessage);
    }

    public void base64ToFile(String destPath,String base64, String fileName) {
        File file = null;
        //创建文件目录
        String filePath=destPath;
        File  dir=new File(filePath);
        if (!dir.exists() && !dir.isDirectory()) {
            dir.mkdirs();
        }
        BufferedOutputStream bos = null;
        java.io.FileOutputStream fos = null;
        try {
            base64 = base64.replaceAll("\r|\n", "");
            base64 = base64.trim();
            byte[] bytes = Base64.getDecoder().decode(base64);
            file=new File(filePath+"/"+fileName);
            fos = new java.io.FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
