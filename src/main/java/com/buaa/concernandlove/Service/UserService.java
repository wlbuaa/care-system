package com.buaa.concernandlove.Service;

import com.buaa.concernandlove.dao.UserDao;
import com.buaa.concernandlove.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String stuId) throws UsernameNotFoundException {
        User user=userDao.getUserByStuId(stuId);
        List<SimpleGrantedAuthority> list=new ArrayList<>();
        if (user!=null){
            list.add(new SimpleGrantedAuthority(user.getUserRole()));
        }else {
            throw new UsernameNotFoundException("用户不存在");
        }
        return new org.springframework.security.core.userdetails.User(user.getUserStuId(),user.getUserPassword(),list);
    }
    public int addUser(User user){
        return userDao.addUser(user);
    }
    public User getUserByStuId(String stuId){
        return userDao.getUserByStuId(stuId);
    }

    public List<Map<String,Object>> getReporterByPage(int pageNum, int pageSize) {
        return userDao.getReporterByPage(pageNum,pageSize);
    }

    public int getPageNum() {
        return userDao.getReporterPageNum();
    }

    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    public List<Map<String, Object>> searchReporter(String keyWord, int pagenum, int pagesize) {
        String where=" where username like \'%"+keyWord+"%\' and userRole!='admin' ";
        String limit=" limit "+(pagenum-1)*pagesize+","+pagesize;
        return userDao.searchReporterByPage(where,limit);
    }
    public int getSearchPageNum(String keyWord){
        String where=" where username like \'%"+keyWord+"%\' and userRole!='admin' ";
        return userDao.getSearchResultNum(where);

    }
    public User getUserById(int userId){
        return userDao.getUserById(userId);
    }
    public void deleteReporterById(int id) {
        userDao.deleteReporterById(id);
    }

    public void banReporterById(int id) {
        userDao.banReporterById(id);
    }
    public List<User> getReportersByWatchedStudentId(String watchedStudentId){
        return userDao.getReportersByWatchedStudentId(watchedStudentId);//这里的watchedStudentId是学工号
    }

    public void cancelBanById(int id) {
        userDao.cancelBanById(id);
    }

    public List<Map<String,Object>> getAllReporterInfo(){
        return userDao.getAllReporterInfo();
    }

    public void dailyUpdate(){
        userDao.dailyUpdateStartAndEndOfReporters();
    }

    public void monthlyUpdate(){
        userDao.monthlyUpdateStartAndEndOfReporters();
    }

    public void weeklyUpdate(){
        userDao.weeklyUpdateStartAndEndOfReporters();
    }

    public String getReporterNameById(int reporterId) {
        return userDao.getReporterNameById(reporterId);
    }
}
