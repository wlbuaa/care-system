package com.buaa.concernandlove;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableScheduling
@EnableSwagger2
@SpringBootApplication
public class ConcernAndLoveApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConcernAndLoveApplication.class, args);
    }

}
