package com.buaa.concernandlove.Controller;

import com.buaa.concernandlove.Service.UserService;
import com.buaa.concernandlove.pojo.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Api(description = "上报员的增删改查")
@CrossOrigin
@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @ApiOperation("增加上报员")
    @PostMapping("/report/add_new_reporter")
    @ResponseBody
    public Map<String,Object> addReporter(@ApiParam("用户名") @RequestParam("name")String username,
                                            @ApiParam("学工号")@RequestParam("id")String stuId,
                                            @ApiParam("电话号")@RequestParam("phone")String phoneNum,
                                            @ApiParam("密码")@RequestParam("password")String password){
        Map<String,Object> map=new HashMap<>();
        if (userService.getUserByStuId(stuId)!=null){
            map.put("code",-1);
            map.put("msg","学工号重复！");
            return map;
        }
        User user=new User(username,stuId,passwordEncoder.encode(password),phoneNum,"user",password, false);
        userService.addUser(user);
        map.put("code",200);
        map.put("msg","添加成功!");
        return map;
    }
    @ApiOperation("分页获取所有上报员")
    @GetMapping("/report/get_all")
    @ResponseBody
    public Map<String,Object> getUserList(@RequestParam("pagenum")int pageNum,@RequestParam("pagesize")int pageSize){
        Map<String,Object> map=new HashMap<>();
        map.put("reporter",userService.getReporterByPage(pageNum,pageSize));
        map.put("total_num",userService.getPageNum());
        map.put("code",200);
        map.put("msg","OK");
        return map;
    }

    @ApiOperation("修改上报员信息,id记得是字符串")
    @PostMapping(value = "/report/edit")
    @ResponseBody
    public Map<String,Object> updateUser(@RequestParam("id") String userid,
                                         @RequestParam("phone")String phone,
                                         @RequestParam("password")String password,
                                         @RequestParam("name")String name){
        Map<String,Object> map=new HashMap<>();
        User user=userService.getUserByStuId(userid);
        if (user==null){
            map.put("code",404);
            map.put("msg","不存在");
            return map;
        }
        user.setUserPhoneNumber(phone);
        user.setUserPasswordRaw(password);
        user.setUserPassword(passwordEncoder.encode(password));
        user.setUsername(name);
        userService.updateUser(user);
        map.put("code",200);
        map.put("msg","修改成功");
        return map;
    }

    @ApiOperation("搜索上报员")
    @GetMapping("/report/search")
    @ResponseBody
    public Map<String,Object> searchReporter(@RequestParam("key")String keyWord,
                                             @RequestParam("pagenum")int pagenum,
                                             @RequestParam("pagesize")int pagesize){
        Map<String,Object> map=new HashMap<>();
        map.put("code",200);
        map.put("msg","OK");
        map.put("reporter",userService.searchReporter(keyWord,pagenum,pagesize));
        map.put("total_num",userService.getSearchPageNum(keyWord));
        return map;
    }


    @ApiOperation("删除上报员,id是学号")
    @GetMapping("/report/delete")
    @ResponseBody
    public Map<String,Object> deleteReporter(@RequestParam("id")int id){
        userService.deleteReporterById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("code",200);
        map.put("msg","删除成功");
        return map;
    }

    @ApiOperation("禁用上报员")
    @GetMapping("/report/ban")
    @ResponseBody
    public Map<String,Object> banReporter(@RequestParam("id")int id){
        userService.banReporterById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("code",200);
        map.put("msg","禁用成功");
        return map;
    }

    @ApiOperation("取消禁用")
    @GetMapping("/report/cancel")
    @ResponseBody
    public Map<String,Object> cancelBanReporter(@RequestParam("id")int id){
        userService.cancelBanById(id);
        Map<String,Object> map=new HashMap<>();
        map.put("code",200);
        map.put("msg","取消成功");
        return map;
    }
    @ApiOperation("获取所有上报员，不分页")
    @GetMapping("/report/getall")
    @ResponseBody
    public Map<String,Object> getAllReporter(){
        Map<String,Object> map=new HashMap<>();
        map.put("code",200);
        map.put("reporter",userService.getAllReporterInfo());
        map.put("msg","OK");
        return map;
    }
}
