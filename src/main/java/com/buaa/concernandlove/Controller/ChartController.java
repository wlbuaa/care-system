package com.buaa.concernandlove.Controller;

import com.buaa.concernandlove.Service.ChartService;
import com.buaa.concernandlove.Service.EmailService;
import com.buaa.concernandlove.Service.ScheduleService;
import com.buaa.concernandlove.Service.WatchedStudentService;
import com.buaa.concernandlove.dao.ChartDao;
import com.buaa.concernandlove.pojo.ReportRecord;
import com.buaa.concernandlove.utils.CalendarUtil;
import com.buaa.concernandlove.utils.JsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Api("报表管理")
@CrossOrigin
@Controller
public class ChartController {
    @Autowired
    ChartService chartService;
    @Autowired
    ChartDao chartDao;

    // Spring里面，@Service对象默认创建单例
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    WatchedStudentService watchedStudentService;
    ScheduleService scheduleService;

    @ApiOperation("查询报表管理")
    @GetMapping("/report_forms/manage")
    @ResponseBody
    public Map<String, Object> getChartManage() {
        return chartService.getChartManage();
    }

    @ApiOperation("设置报表推送规则")
    @PostMapping("/report_forms/manage")
    @ResponseBody
    public String setChartManage(@RequestBody Map<String, Object> params) {
        System.out.println(params);
        int level1_count = (int) params.get("level_1_count");
        int level2_count = (int) params.get("level_2_count");
        int range_time = (int) params.get("range_time");
        List<String> mailList=(List<String>) params.get("mail");
        StringBuilder sb=new StringBuilder();
        for(String oneMail:mailList){
            sb.append(oneMail+",");
        }
        String mail=sb.substring(0,sb.length()-1);
        List<String> level1_time = JsonUtil.ObjectToList(params.get("level_1_time"));
        List<String> level2_time = JsonUtil.ObjectToList(params.get("level_2_time"));

        //以下为新加的字段
        int catch_count = (int) params.get("catch_count");
        List<String> catch_time = JsonUtil.ObjectToList(params.get("catch_time"));
        String phone = (String) params.get("phone");
        int time_interval = (int) params.get("interval");

        System.out.println(level2_time);
        if(chartService.setChartManage(level1_count, level2_count, level1_time, level2_time, range_time, mail,
                catch_count, catch_time, phone, time_interval) == 1) {
            return "设置成功!";
        }
        else return "设置失败!";
    }

    @ApiOperation("报表预览接口")
    @GetMapping("/chart/preview")
    @ResponseBody
    public Map<String,Object> GetReportPreview(@RequestParam("student_id")String studentId, @RequestParam("week")int week){
        Map<String,Object> map=new HashMap<>();
        Calendar firstDate = CalendarUtil.getFirstDay(week);
        Calendar lastDate = Calendar.getInstance();
        lastDate.setTime(firstDate.getTime());
        SimpleDateFormat Md = new SimpleDateFormat("MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat Hm = new SimpleDateFormat("HH:mm");
//        Map<String,Object> watchedStudentMap=watchedStudentService.getWatchedStudentById(studentId).get(0);
//        String stuId=watchedStudentMap.get("stuId").toString();
        List<String> date = new ArrayList<>();
        for(int i=0;i<7;i++){
            String day = Md.format(lastDate.getTime());
            System.out.println(day);
            date.add(day);
            lastDate.add(Calendar.DATE,1);
        }
        Calendar now = Calendar.getInstance();
        System.out.println(sdf.format(firstDate.getTime()));
        System.out.println(sdf.format(lastDate.getTime()));
        System.out.println(sdf.format(now.getTime()));
        // 当前时间比开始时间早，没有打卡数据
        if(now.before(firstDate)){
            map.put("success",false);
            map.put("exc","暂无此周数据");
        }
        else {
            Calendar start=Calendar.getInstance();
            Calendar end=Calendar.getInstance();
            if (now.before(lastDate)) {
                start.setTime(firstDate.getTime());
                end.setTime(now.getTime());
            } else {
                start.setTime(firstDate.getTime());
                end.setTime(lastDate.getTime());
            }
            Map<String,Object> data=new HashMap<>();
            List<String> clockTime = new ArrayList<>();
            List<Double> score = new ArrayList<>();
            List<Integer> onTime = new ArrayList<>();
            List<String> backTime = new ArrayList<>();
            List<Integer> posChange = new ArrayList<>();
            Calendar tmp = Calendar.getInstance();
            tmp.setTime(start.getTime());
            int stuId = Integer.parseInt(watchedStudentService.getWatchedStudentById(studentId).get(0).get("studentId").toString());
            while(start.before(end)){
                tmp.add(Calendar.DATE,1);

                // 获取start那天的提交记录
                List<ReportRecord> reportRecords=chartService.getReportsBetween(stuId,
                        sdf.format(start.getTime()),sdf.format(tmp.getTime()));
                double ave=0;
                int num=0;
                int onTimeRecordNum=0;
                for(ReportRecord reportRecord:reportRecords){
                    System.out.println(reportRecord.getRecordId());
                    ave+=chartService.countScoresByReportRecord(reportRecord.getRecordId());
                    onTimeRecordNum+=chartService.isReportOnTime(reportRecord.getReporterId(),reportRecord.getStudentId());
                    num++;
                }
                if(num>0) ave /= num;
                score.add(ave);
                onTime.add(onTimeRecordNum);
                Calendar oneWeekBefore=Calendar.getInstance();
                oneWeekBefore.setTime(end.getTime());
                oneWeekBefore.add(Calendar.DATE,-7);
                int moveTimes=chartService.countMovesBetween(studentId, sdf.format(oneWeekBefore.getTime()),
                        sdf.format(tmp.getTime()));
                posChange.add(moveTimes);
                System.out.println("["+sdf.format(start.getTime())+","+sdf.format(tmp.getTime())+"]");
                Timestamp clockTimeStamp=chartService.getClockTime(studentId,sdf.format(start.getTime()),sdf.format(tmp.getTime()));
                if (clockTimeStamp!=null)clockTime.add(Hm.format(clockTimeStamp));
                else clockTime.add("");
                start.add(Calendar.DATE,1);
            }
            data.put("clock_time",clockTime);
            data.put("score",score);
            data.put("on_time",onTime);
            data.put("back_time",backTime);
            data.put("position_change",posChange);
            map.put("data",data);
            map.put("date",date);
            map.put("success",true);
            map.put("exc","");
            map.put("mail",chartService.getChartManage().get("mail"));
//            try {
//                System.out.println(chartService.getChartManage().get("mail").toString());
//                map.put("mail", chartService.getChartManage().get("mail").toString().split(","));
//            }
//            catch (Exception e){
//                map.put("mail","");
//            }
//            String start, end;
//            if (now.before(lastDate)) {
//                start = sdf.format(firstDate.getTime());
//                end = sdf.format(now.getTime());
//            } else {
//                start = sdf.format(firstDate.getTime());
//                end = sdf.format(lastDate.getTime());
//            }
//            List<ReportRecord> reportRecords = chartService.getReportsBetween(studentId,start,end);
//            if(reportRecords==null){
//                map.put("success",false);
//                map.put("exc","暂无此周数据");
//            }
//            else {
//                Map<String,List<?>> data = new HashMap<>();
//                List<String> clockTime = new ArrayList<>();
//                List<Double> score = new ArrayList<>();
//                List<Boolean> onTime = new ArrayList<>();
//                List<String> backTime = new ArrayList<>();
//                List<Integer> posChange = new ArrayList<>();
//                for(ReportRecord reportRecord:reportRecords){
//                    clockTime.add(sdf.format(reportRecord.getReportTime()));
//                }
//                data.put("clock_time",clockTime);
//                data.put("score",score);
//                data.put("on_time",onTime);
//                data.put("back_time",backTime);
//                data.put("position_change",posChange);
//                map.put("data",data);
//                map.put("date",date);
//                map.put("success",true);
//                map.put("exc","");
//            }
        }
        return map;
    }

    @Autowired
    EmailService emailService;

    @ApiOperation("报表推送")
    @PostMapping("/send_pdf")
    @ResponseBody
    public Map<String,Object> sendPDF(@RequestBody Map<String,Object> params){
        String emailTo = params.get("email").toString();
        String base64 = params.get("base64").toString();
        String name = params.get("name").toString();
        String fileName = "chart.pdf";
        String filePath = ".";
        emailService.base64ToFile(filePath,base64,fileName);
        String subject = name+"的推送报告"; // 标题
        String content = "这是"+name+"今日的报告推送，请及时查看";  // 内容
        Map<String,Object> map=new HashMap<>();
        try{
            emailService.SendEmail(emailTo,subject,content,new File(filePath+"/"+fileName));
            map.put("success",true);
            map.put("exc","");
        } catch (Exception e){
            map.put("success",false);
            map.put("exc",e.toString());
        }
        return map;
    }


}
