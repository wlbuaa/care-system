package com.buaa.concernandlove.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.buaa.concernandlove.Service.QuestionService;
import com.buaa.concernandlove.pojo.Question;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Api(description = "题目的增删改查")
@CrossOrigin
@Controller
public class QuestionController {
    @Autowired
    private QuestionService questionService;
    @ApiOperation("添加题目")
    @PostMapping("/question/add")
    @ResponseBody
    public Map<String,Object> addQuestion(@ApiParam(example = "{\n" +
            "    \"type\":1,\n" +
            "    \"title\":\"题干\",\n" +
            "    \"required\":0,\n" +
            "    \"options\":[\"选项1\",\"选项2\",\"选项3\"]\n" +
            "}") @RequestBody JSONObject jsonObject){
//        int userid=Integer.parseInt( (String) SecurityContextHolder.getContext().getAuthentication().getDetails());
        int userid=1;
        int type= jsonObject.getIntValue("type");
        int isMust=jsonObject.getBoolean("required")?1:0;
        String title=jsonObject.getString("title");
        JSONArray choices=jsonObject.getJSONArray("options");
        Question question=new Question(userid,type,title,isMust,choices);
        questionService.addQuestion(question);
        Map<String,Object> map=new HashMap<>();
        map.put("code",200);
        map.put("msg","添加成功");
        return map;
    }
    @PostMapping("/question/delete")
    @ApiOperation("批量删除题目")
    @ResponseBody
    public Map<String,Object> deleteQuestions(@ApiParam(example = "{\n" +
            "  \"list\":[1,2,3]\n" +
            "}")@RequestBody JSONObject jsonObject){
        JSONArray list=jsonObject.getJSONArray("list");
        Map<String,Object> map=new HashMap<>();
        if (list.size()==0){
            map.put("code",-1);
            map.put("msg","列表不能为空");
            return map;
        }
        questionService.deleteQuestions(list);
        map.put("code",200);
        map.put("msg","删除成功");
        return map;
    }

    @ApiOperation("获取题目列表")
    @GetMapping(value = "/question/list")
    @ResponseBody
    public Map<String,Object> getQuestionList(@RequestParam("start_time")Long startTime,
                                              @RequestParam("end_time")Long endTime,
                                              @RequestParam("pagenum")int pageNum,
                                              @RequestParam("pagesize")int pageSize,
                                              @RequestParam("required")int isMust,
                                              @RequestParam("type")int type,
                                              @RequestParam("title")String titleKeyWord){
        Timestamp start_time=new Timestamp(startTime);
        Timestamp end_time=new Timestamp(endTime);
        Map<String,Object> map=new HashMap<>();
        map.put("list",questionService.getQuestionList(pageSize,pageNum,titleKeyWord,isMust,type,start_time,end_time));
        map.put("total",questionService.getQuestionListPageNum(titleKeyWord,isMust,type,start_time,end_time));
        map.put("code",200);
        return map;
    }

    @ApiOperation("学生获取题库的题目")
    @GetMapping("/stu/findq")
    @ResponseBody
    public Map<String,Object> getQuestionStu(@RequestParam("type")int type,
                                             @RequestParam("required")int required,
                                             @RequestParam("title")String keyWord){
        Map<String, Object> map = new HashMap<>();
        try {
            map.put("list", questionService.getQuestionListStu(type, required, keyWord));
            map.put("success", true);
        }catch (Exception e){
            map.put("success",false);
            map.put("exc","出现了奇怪的bug");
            e.printStackTrace();
        }
        return map;
    }
    @ApiOperation("修改题目")
    @PostMapping("/question/edit")
    @ResponseBody
    public Map<String,Object> updateQuestion(@RequestBody JSONObject jsonObject){
        int userid=1;
        int type= jsonObject.getIntValue("type");
        int isMust=jsonObject.getBoolean("required")?1:0;
        String title=jsonObject.getString("title");
        JSONArray choices=jsonObject.getJSONArray("options");
        Question question=new Question(userid,type,title,isMust,choices);
        question.setQuestionId(jsonObject.getInteger("id"));
        questionService.updateQuestion(question);
        Map<String,Object> map=new HashMap<>();
        map.put("code",200);
        map.put("msg","修改成功");
        return map;
    }
    @ApiOperation("获取题目被使用次数")
    @GetMapping("/question/count")
    @ResponseBody
    public Map<String ,Object> countQuestion(@RequestParam("stuid")String studentId,@RequestParam("qid")int questionId){
        Map<String,Object> map=new HashMap<>();
        try {

        map.put("count",questionService.getQuestionCount(studentId, questionId));
        map.put("code",200);
        }catch (Exception e){
            map.put("count",1);
            map.put("code",200);
        }
        return map;
    }
}
