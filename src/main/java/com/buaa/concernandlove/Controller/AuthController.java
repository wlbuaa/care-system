package com.buaa.concernandlove.Controller;

import com.buaa.concernandlove.Service.UserService;
import com.buaa.concernandlove.pojo.User;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin
@RestController
@Api(description = "who am i")
public class AuthController {
    @Autowired
    private UserService userService;
    @GetMapping("/whoami")
    public Map<String,Object> whoAmI(){
        int userid=Integer.parseInt( (String) SecurityContextHolder.getContext().getAuthentication().getDetails());
        User user=userService.getUserById(userid);
        Map<String,Object> map=new HashMap<>();
        map.put("code",200);
        map.put("data",user);
        return map;
    }
}
