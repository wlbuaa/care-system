package com.buaa.concernandlove.Controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.buaa.concernandlove.Service.QuestionService;
import com.buaa.concernandlove.Service.WarningService;
import com.buaa.concernandlove.Service.WatchedStudentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/warning_stu")
@CrossOrigin(origins = "*")
public class WarningController {

    @Autowired
    WarningService warningService;

    @Autowired
    WatchedStudentService watchedStudentService;

    @Autowired
    QuestionService questionService;

    @PostMapping("/add")
    @ApiOperation("添加预警学生")
    Map<String, Object> addWarningStu(@RequestBody Map<String, Object> param) {
        Map<String, Object> res = new HashMap<>();
        String name = null;
        String id = null;
        try {
            name = param.get("name").toString();
            id = param.get("id").toString();
            List<Map<String, Object>> stu = watchedStudentService.getWatchedStudentById(id);
            if (stu == null || stu.isEmpty()) {
                res.put("code", -1);
                res.put("msg", "学生学号不存在");
                return res;
            }
            String studentId = stu.get(0).get("studentId").toString();
            int flag = warningService.addWarningStu(Integer.parseInt(studentId));
            if (flag == -1) {
                res.put("code", -1);
                res.put("msg", "学生预警已添加");
                return res;
            }
            res.put("code", 200);
            res.put("msg", "添加成功");
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            res.put("code", -1);
            res.put("msg", "参数异常");
            return res;
        }
    }

    @PostMapping("/delete")
    @ApiOperation("删除预警学生")
    Map<String, Object> deleteWarningStu(@RequestBody Map<String, Object> param) {
        Map<String, Object> res = new HashMap<>();
        String id = null;
        try {
            id = param.get("id").toString();
            List<Map<String, Object>> stu = watchedStudentService.getWatchedStudentById(id);
            if (stu == null || stu.isEmpty()) {
                res.put("code", -1);
                res.put("msg", "学生学号不存在");
                return res;
            }
            String studentId = stu.get(0).get("studentId").toString();
            int flag = warningService.deleteWarningStu(Integer.parseInt(studentId));
            if (flag == -1) {
                res.put("code", -1);
                res.put("msg", "学生不在预警列表");
                return res;
            }
            res.put("code", 200);
            res.put("msg", "删除成功");
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            res.put("code", -1);
            res.put("msg", "参数异常");
            return res;
        }
    }

    @PostMapping("/disable")
    @ApiOperation("改变预警学生禁用状态")
    Map<String, Object> warningStuStatus(@RequestBody Map<String, Object> param) {
        Map<String, Object> res = new HashMap<>();
        String id = null;
        try {
            id = param.get("id").toString();
            List<Map<String, Object>> stu = watchedStudentService.getWatchedStudentById(id);
            if (stu == null || stu.isEmpty()) {
                res.put("code", -1);
                res.put("msg", "学生学号不存在");
                return res;
            }
            String studentId = stu.get(0).get("studentId").toString();
            int flag = warningService.changeWarningStu(Integer.parseInt(studentId));
            if (flag == -1) {
                res.put("code", -1);
                res.put("msg", "学生不在预警列表");
                return res;
            }
            res.put("code", 200);
            res.put("msg", "修改学生状态成功");
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            res.put("code", -1);
            res.put("msg", "参数异常");
            return res;
        }
    }

    @GetMapping("/list")
        //页面从0开始
    @ApiOperation("分页获取预警学生列表")
    Map<String, Object> getPagedWarningStu(@RequestParam("pagenum") int pageNum, @RequestParam("pagesize") int pageSize) {
        Map<String, Object> res = new HashMap<>();
        res.put("list", warningService.getWarningStu(pageNum, pageSize));
        res.put("code", 200);
        res.put("msg", "查询成功");
        return res;
    }

    @GetMapping("/log")
    @ApiOperation("分页获取学生预警记录")
    Map<String, Object> getPagedWarningStu(@RequestParam("id") String id, @RequestParam("pagenum") int pageNum, @RequestParam("pagesize") int pageSize) {
        Map<String, Object> res = new HashMap<>();
        List<Map<String, Object>> stu = watchedStudentService.getWatchedStudentById(id);
        if (stu == null || stu.isEmpty()) {
            res.put("code", -1);
            res.put("msg", "学生学号不存在");
            return res;
        }
        String studentId = stu.get(0).get("studentId").toString();
        res.put("list", warningService.getWarningStuLog(Integer.parseInt(studentId), pageNum, pageSize));
        res.put("code", 200);
        res.put("msg", "查询成功");
        return res;
    }

    @PostMapping("/edit")
    @ApiOperation("修改学生预警条件")
    Map<String, Object> editStuWarning(@RequestBody JSONObject param) {

        Map<String, Object> res = new HashMap<>();
        String id = null;
        List<JSONObject> info = new ArrayList<>();
        try {
            id = param.get("id").toString();
            List<Map<String, Object>> stu = watchedStudentService.getWatchedStudentById(id);
            if (stu == null || stu.isEmpty()) {
                res.put("code", -1);
                res.put("msg", "学生学号不存在");
                return res;
            }
            String studentId = stu.get(0).get("studentId").toString();
            for (int i = 1; i <= 3; i++){
                LinkedHashMap tmp =  (LinkedHashMap) param.get("warning_"+i);
                JSONObject list = new JSONObject();
                list.put("question", tmp.get("question"));
                list.put("reporter", tmp.get("reporter"));
                list.put("insideRelation",tmp.get("insideRelation"));
                info.add(list);
            }
            for (int i = 1; i <= 3; i++){
                LinkedHashMap tmp =  (LinkedHashMap) param.get("warningex_"+i);
                JSONObject list = new JSONObject();
                list.put("question", tmp.get("question"));
                list.put("reporter", tmp.get("reporter"));
                list.put("insideRelation",tmp.get("insideRelation"));
                info.add(list);
            }
            int flag = warningService.updateStuWarning(Integer.parseInt(studentId), info.get(0).toString(), info.get(1).toString(), info.get(2).toString(), info.get(3).toString(), info.get(4).toString(), info.get(5).toString());
            if (flag == -1) {
                res.put("code", -1);
                res.put("msg", "学生不在预警列表");
                return res;
            }
            res.put("code", 200);
            res.put("msg", "修改成功");
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            res.put("code", -1);
            res.put("msg", "参数异常");
            return res;
        }
    }

    @GetMapping("/detail")
    @ApiOperation("获取学生预警详情")
    Map<String, Object> stuWarningDetail(@RequestParam("id")String id) {

        Map<String, Object> res = new HashMap<>();
        try {
            List<Map<String, Object>> stu = watchedStudentService.getWatchedStudentById(id);
            if (stu == null || stu.isEmpty()) {
                res.put("code", -1);
                res.put("msg", "学生学号不存在");
                return res;
            }
            String studentId = stu.get(0).get("studentId").toString();
            Map<String, Object> detail = warningService.getStuWarningDetail(Integer.parseInt(studentId));
            if (detail == null) {
                res.put("code", -1);
                res.put("msg", "学生不在预警列表");
                return res;
            }
            for (int i = 1; i <= 3; i++){
                JSONObject tmp = JSONObject.parseObject(detail.get("warningCondition"+i).toString());
                if (tmp == null || tmp.isEmpty()) {
                    res.put("warning_"+i, null);
                    continue;
                }
                res.put("warning_"+i, tmp);
            }
            for (int i = 1; i <= 3; i++) {
                JSONObject tmp = JSONObject.parseObject(detail.get("warningConditionEx"+i).toString());
                if (tmp == null || tmp.isEmpty()) {
                    res.put("warningex_"+i, null);
                    continue;
                }
                res.put("warningex_"+i, tmp);
            }
            JSONObject rq = warningService.getReporterQuestion(Integer.parseInt(studentId));
            res.put("code", 200);
            res.put("msg", "修改成功");
            res.put("question", rq.get("question"));
            res.put("reporter", rq.get("reporter"));
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            res.put("code", -1);
            res.put("msg", "参数异常");
            return res;
        }
    }

    @PostMapping("/log-solved")
    @ApiOperation("分页获取学生预警记录")
    Map<String, Object> solveLog(@RequestBody Map<String,Object> params) {
        String id=params.get("id").toString();
        int logid=(Integer)params.get("index");
        Map<String, Object> res = new HashMap<>();
        List<Map<String, Object>> stu = watchedStudentService.getWatchedStudentById(id);
        if (stu == null || stu.isEmpty()) {
            res.put("code", -1);
            res.put("msg", "学生学号不存在");
            return res;
        }
        String studentId = stu.get(0).get("studentId").toString();
        warningService.solve(Integer.parseInt(studentId), logid);
        res.put("code", 200);
        res.put("msg", "解除预警成功");
        return res;
    }
}
