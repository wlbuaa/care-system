package com.buaa.concernandlove.Controller;

import com.alibaba.fastjson.JSONObject;
import com.buaa.concernandlove.Service.ReportService;
import com.buaa.concernandlove.Service.ScheduleService;
import com.buaa.concernandlove.Service.UserService;
import com.buaa.concernandlove.Service.WatchedStudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Api(description = "填报的接口，需要登录之后再请求喔")
@Controller
public class UpReportController {

    @Autowired
    private UserService userService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private WatchedStudentService watchedStudentService;

    @ApiOperation("获取监护人监护的学生列表")
    @ResponseBody
    @GetMapping("/report/students")
    public Map<String,Object> getStudents(){
        Map<String,Object> map=new HashMap<>();
        String userStuId= SecurityContextHolder.getContext().getAuthentication().getName();
        map.put("list",watchedStudentService.getWatchedStudentList(userStuId));
        map.put("code",200);
        return map;
    }
    @ApiOperation("根据学生id获取一张填报试卷")
    @ResponseBody
    @GetMapping("/report/paper")
    public Map<String,Object> getPaper(@RequestParam("id")String studentStuId){
        String userStuId= SecurityContextHolder.getContext().getAuthentication().getName();
        return  watchedStudentService.getPaperByReporterAndStudent(userStuId,studentStuId);
    }
    @ApiOperation("上传填报")
    @ResponseBody
    @PostMapping("/report/upload")
    public Map<String,Object> addReport(@RequestBody JSONObject jsonObject){
        int userid=Integer.parseInt( (String) SecurityContextHolder.getContext().getAuthentication().getDetails());
        Map<String,Object> map=new HashMap<>();
        String studentStuId=jsonObject.getString("student_id");
        if (new Timestamp(System.currentTimeMillis()).before(watchedStudentService.getStartTime(studentStuId,userid))){
            map.put("code",-1);
            return map;
        }
        reportService.addReport(jsonObject, userid);
        int studentId= (int) watchedStudentService.getWatchedStudentById(jsonObject.getString("student_id")).get(0).get("studentId");
        watchedStudentService.updateWatchedStudentReported(studentId);//每上报一次，就更新学生的reported
        map.put("code",200);
        return map;
    }

}
