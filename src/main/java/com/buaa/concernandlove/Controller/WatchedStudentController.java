package com.buaa.concernandlove.Controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.buaa.concernandlove.Service.UserService;
import com.buaa.concernandlove.Service.WatchedStudentService;
import com.buaa.concernandlove.config.GlobalVariable;
import com.buaa.concernandlove.pojo.Couriers;
import com.buaa.concernandlove.pojo.WatchedStudentDetail;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

@Api(description = "学生管理")
@CrossOrigin
@Controller
public class WatchedStudentController {
    @Autowired
    WatchedStudentService watchedStudentService;
    @Autowired
    GlobalVariable globalVariable;
    @Autowired
    UserService userService;
    @ResponseBody
    @ApiOperation("添加学生")
    @PostMapping("/stu/add")
    public Map<String,Object> addSWatchedStudent(@RequestBody JSONObject jsonObject) throws IOException {
        System.out.println(jsonObject);
        WatchedStudentDetail watchedStudentDetail = new WatchedStudentDetail();
        watchedStudentDetail.setStudentAvatarUrl(jsonObject.getString("photo"));
        watchedStudentDetail.setStudentName(jsonObject.getString("name"));
        watchedStudentDetail.setStuId(jsonObject.getString("num"));
        watchedStudentDetail.setDormNum(jsonObject.getString("dorm"));
        watchedStudentDetail.setPhoneNum(jsonObject.getString("tel"));
        watchedStudentDetail.setSex(jsonObject.getString("sex"));
        watchedStudentDetail.setHomeAddress(jsonObject.getString("address"));
        watchedStudentDetail.setGrade(jsonObject.getString("grade"));
        watchedStudentDetail.setClassNum(jsonObject.getString("class"));
        watchedStudentDetail.setClassMonitorName(jsonObject.getJSONObject("monitor").getString("name"));
        watchedStudentDetail.setClassMonitorPhoneNum(jsonObject.getJSONObject("monitor").getString("tel"));
        watchedStudentDetail.setMentorName(jsonObject.getJSONObject("tutor").getString("name"));
        watchedStudentDetail.setMentorPhoneNum(jsonObject.getJSONObject("tutor").getString("tel"));
        watchedStudentDetail.setEmergencyContactName(jsonObject.getJSONObject("contact").getString("name"));
        watchedStudentDetail.setEmergencyContactPhoneNum(jsonObject.getJSONObject("contact").getString("tel"));
        watchedStudentDetail.setCounselorName(jsonObject.getJSONObject("instructor").getString("name"));
        watchedStudentDetail.setCounselorPhoneNum(jsonObject.getJSONObject("instructor").getString("tel"));
        watchedStudentDetail.setConcernLevel(jsonObject.getString("rank"));
        watchedStudentDetail.setConcernReason(jsonObject.getString("reason"));
        watchedStudentDetail.setSpecialCondition(jsonObject.getString("special"));
        System.out.println(watchedStudentDetail);
        JSONArray jsonArray = jsonObject.getJSONArray("couriers");
        List<Couriers> list = new ArrayList<>();
        for(int i=0;i<jsonArray.size();i++){
            Couriers couriers = new Couriers((JSONObject) jsonArray.get(i));
            if (userService.getUserByStuId(jsonArray.getJSONObject(i).getString("num"))==null){
                Map<String,Object> map = new HashMap<>();
                map.put("success",false);
                map.put("exc","不存在学号为"+jsonArray.getJSONObject(i).getString("num")+"的上报员");
                return map;
            }
            list.add(couriers);
        }
        watchedStudentDetail.setCouriersList(list);
        Map<String,Object> map = new HashMap<>();
        if(watchedStudentService.addWatchedStudent(watchedStudentDetail)==-1){
            map.put("success",false);
            map.put("exc","该学生已存在");
        }
        else{
            map.put("success",true);
            map.put("exc","");
        }
        return map;
    }
    @ApiOperation("上传学生图片，返回存储路径")
    @ResponseBody
    @PostMapping("/stu/upload")
    public Map<String,Object> uploadStudentPhoto(@RequestParam("photo")MultipartFile uploadFile) throws IOException {
//        String realPath="/usr/local/nginx/html/img/avatar";
        String realPath="/www/wwwroot/39.105.189.194/img/avatar";//部署在地质主机的版本
        Map<String,Object> map=new HashMap<>();
        File folder=new File(realPath);
        if (!folder.isDirectory()){
            folder.mkdirs();
        }
        String oldName = uploadFile.getOriginalFilename();
        if (!oldName.equals("")){
            String newName= UUID.randomUUID().toString()+oldName.substring(oldName.lastIndexOf("."));
            uploadFile.transferTo(new File(folder,newName));
            String picPath='/'+newName;
            map.put("avatar",picPath);
        }
        map.put("success",true);
        map.put("exc","");
        return map;
    }
    @ResponseBody
    @ApiOperation("根据姓名搜索学生列表，分页查看")
    @GetMapping("/stu/list")
    public Map<String,Object> doSearch(@RequestParam("name")String name,@RequestParam("pagenum")int pagenum,@RequestParam("pagesize")int pagesize){
        Map<String,Object> map = new HashMap<>();
        List<Map<String,Object>> list;
        int total=0;
        if(name.length()==0){
            list = watchedStudentService.getAllStudentByPage(pagenum,pagesize);
            total = watchedStudentService.getAllStudentPageNum();
        }
        else{
            list = watchedStudentService.getKeyStudentByPage(pagenum,pagesize,name);
            total = watchedStudentService.getKeyStudentPageNum(name);
        }
        map.put("success",true);
        map.put("exc","");
        map.put("list",list);
        map.put("total",total);
        return map;
    }
    @ResponseBody
    @ApiOperation("获取上网数据的接口")
    @GetMapping("/network")
    public Map<String,Object> getNetworkInfo(@RequestParam("student_id")int id){
        Map<String,Object> map = new HashMap<>();
        Map<String,Object> data = new HashMap<>();
        data.put("time","");
        data.put("router_id","");
        data.put("router_ip","");
        data.put("user_ip","");
        data.put("website","");
        data.put("keyword","");
        map.put("data",data);
        map.put("success",true);
        map.put("exc","");
        return map;
    }
    @ResponseBody
    @ApiOperation("获取出入校信息的接口")
    @GetMapping("/school")
    public Map<String,Object> getSchoolInfo(@RequestParam("student_id")int id){
        Map<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("exc","");
        return map;
    }
    @ResponseBody
    @ApiOperation("或许健康上报的接口")
    @GetMapping("/health_report")
    public Map<String,Object> getHealthReport(@RequestParam("student_id")String id){
        Map<String,Object> map = new HashMap<>();
        map.put("list",watchedStudentService.getConvRecord(id));
        map.put("success",true);
        map.put("exc","");
        return map;
    }
    @ResponseBody
    @ApiOperation("获取学生详细信息的接口")
    @GetMapping("/stu/info")
    public Map<String,Object> getStuInfo(@RequestParam("id")int id){
        Map<String,Object> map = new HashMap<>();
        if(watchedStudentService.getWatchedStudentById(id).isEmpty()){
            map.put("success",false);
            map.put("exc","id不存在");
            return map;
        }
        Map<String,Object> tmp = watchedStudentService.getWatchedStudentById(id).get(0);
        System.out.println(tmp);
        map.put("name",tmp.get("studentName"));
        map.put("num",tmp.get("stuId"));
        map.put("dorm",tmp.get("dormNum"));
        map.put("tel",tmp.get("phoneNum"));
        map.put("sex",tmp.get("sex"));
        map.put("address",tmp.get("homeAddress"));
        map.put("grade",tmp.get("grade"));
        map.put("class",tmp.get("classNum"));
        map.put("rank",tmp.get("concernLevel"));
        map.put("reason",tmp.get("concernReason"));
        map.put("special",tmp.get("specialCondition"));
        map.put("photo",tmp.get("studentAvatarUrl"));
        Map<String,Object> monitor = new HashMap<>();
        monitor.put("name",tmp.get("classMonitorName"));
        monitor.put("tel",tmp.get("classMonitorPhoneNum"));
        Map<String,Object> tutor = new HashMap<>();
        tutor.put("name",tmp.get("mentorName"));
        tutor.put("tel",tmp.get("mentorPhoneNum"));
        Map<String,Object> contact = new HashMap<>();
        contact.put("name",tmp.get("emergencyContactName"));
        contact.put("tel",tmp.get("emergencyContactPhoneNum"));
        Map<String,Object> instructor = new HashMap<>();
        instructor.put("name",tmp.get("counselorName"));
        instructor.put("tel",tmp.get("counselorPhoneNum"));
        map.put("monitor",monitor);
        map.put("tutor",tutor);
        map.put("contact",contact);
        map.put("instructor",instructor);
        List<Map<String,Object>> couriers = watchedStudentService.getCouriersById(id);
        for(Map<String,Object> courier:couriers){
            long start = ((Timestamp)courier.get("start")).getTime();
            long end = ((Timestamp)courier.get("end")).getTime();
            courier.remove("start");
            courier.remove("end");
            courier.put("start",start);
            courier.put("end",end);
        }
        map.put("couriers",couriers);
        map.put("success",true);
        map.put("exc","");
        return map;
    }
    @ResponseBody
    @ApiOperation("编辑学生信息的接口")
    @PostMapping("/stu/edit")
    public Map<String,Object> editStuInfo(@RequestBody JSONObject jsonObject) throws IOException {
        WatchedStudentDetail watchedStudentDetail = new WatchedStudentDetail();
        watchedStudentDetail.setStudentId(jsonObject.getIntValue("id"));
        watchedStudentDetail.setStudentName(jsonObject.getString("name"));
        watchedStudentDetail.setStuId(jsonObject.getString("num"));
        watchedStudentDetail.setStudentAvatarUrl(jsonObject.getString("photo"));
        watchedStudentDetail.setDormNum(jsonObject.getString("dorm"));
        watchedStudentDetail.setPhoneNum(jsonObject.getString("tel"));
        watchedStudentDetail.setSex(jsonObject.getString("sex"));
        watchedStudentDetail.setHomeAddress(jsonObject.getString("address"));
        watchedStudentDetail.setGrade(jsonObject.getString("grade"));
        watchedStudentDetail.setClassNum(jsonObject.getString("class"));
        watchedStudentDetail.setClassMonitorName(jsonObject.getJSONObject("monitor").getString("name"));
        watchedStudentDetail.setClassMonitorPhoneNum(jsonObject.getJSONObject("monitor").getString("tel"));
        watchedStudentDetail.setMentorName(jsonObject.getJSONObject("tutor").getString("name"));
        watchedStudentDetail.setMentorPhoneNum(jsonObject.getJSONObject("tutor").getString("tel"));
        watchedStudentDetail.setEmergencyContactName(jsonObject.getJSONObject("contact").getString("name"));
        watchedStudentDetail.setEmergencyContactPhoneNum(jsonObject.getJSONObject("contact").getString("tel"));
        watchedStudentDetail.setCounselorName(jsonObject.getJSONObject("instructor").getString("name"));
        watchedStudentDetail.setCounselorPhoneNum(jsonObject.getJSONObject("instructor").getString("tel"));
        watchedStudentDetail.setConcernLevel(jsonObject.getString("rank"));
        watchedStudentDetail.setConcernReason(jsonObject.getString("reason"));
        watchedStudentDetail.setSpecialCondition(jsonObject.getString("special"));
        JSONArray jsonArray = jsonObject.getJSONArray("couriers");
        List<Couriers> list = new ArrayList<>();
        for(int i=0;i<jsonArray.size();i++){
            Couriers couriers = new Couriers((JSONObject) jsonArray.get(i));
            if (userService.getUserByStuId(jsonArray.getJSONObject(i).getString("num"))==null){
                Map<String,Object> map = new HashMap<>();
                map.put("success",false);
                map.put("exc","不存在学号为"+jsonArray.getJSONObject(i).getString("num")+"的上报员");
                return map;
            }
            list.add(couriers);
        }
        watchedStudentDetail.setCouriersList(list);
        Map<String,Object> map = new HashMap<>();
        if(watchedStudentService.updateStuInfo(watchedStudentDetail)==-1){
            map.put("success",false);
            map.put("exc","id不存在");
        }
        else{
            map.put("success",true);
            map.put("exc","");
        }
        return map;
    }
    @ResponseBody
    @ApiOperation("删除学生")
    @PostMapping("/stu/del")
    public Map<String,Object> deleteStudent(@RequestParam("id")int id){
        Map<String,Object> map = new HashMap<>();
        if (watchedStudentService.deleteStudent(id)==-1){
            map.put("success",false);
            map.put("exc","id不存在");
        }
        else{
            map.put("success",true);
            map.put("exc","");
        }
        return map;
    }
    @ResponseBody
    @ApiOperation("开始追踪")
    @PostMapping("/stu/open")
    public Map<String, Object> trackStudent(@RequestParam("id")int id) {
        Map<String,Object> map = new HashMap<>();
        int influenced_lines = watchedStudentService.trackStudent(id);
        if(influenced_lines == 0) {
            map.put("success", false);
            map.put("exc", "id不存在");
        }
        else {
            map.put("success",true);
            map.put("exc","");
        }
        return map;
    }
    @ResponseBody
    @ApiOperation("停止追踪")
    @PostMapping("/stu/close")
    public Map<String, Object> untrackStudent(@RequestParam("id")int id) {
        Map<String,Object> map = new HashMap<>();
        int influenced_lines = watchedStudentService.untrackStudent(id);
        if(influenced_lines == 0) {
            map.put("success", false);
            map.put("exc", "id不存在");
        }
        else {
            map.put("success",true);
            map.put("exc","");
        }
        return map;
    }
    @ResponseBody
    @ApiOperation("获取上报信息列表")
    @GetMapping("/stu/msg")
    public Map<String,Object> getReportMessage(@RequestParam("id")int id,
                                               @RequestParam("reporterId")int reporterId,
                                               @RequestParam(value = "start",required = false,defaultValue = "0")long start,
                                               @RequestParam(value = "end",required = false)Long end,
                                               @RequestParam("sort")int sort,
                                               @RequestParam("pagenum")int pagenum,
                                               @RequestParam("pagesize")int pagesize){
        //sort0降1升
        Map<String,Object> map = new HashMap<>();
        if (end==null){
            end=System.currentTimeMillis();
        }
        map.put("list",watchedStudentService.getReportMessage(id,reporterId,start,end,sort,pagenum,pagesize));
        map.put("total",watchedStudentService.getReportMessageTotalNum(id,reporterId,start,end));
        map.put("success",true);
        map.put("exc","");
        return map;
    }
    @ResponseBody
    @GetMapping("/stu/couriers")
    @ApiOperation("获取学生所有的上报员")
    public Map<String,Object> getCouriers(@RequestParam("id")int studentId){
        Map<String,Object> map=new HashMap<>();
        map.put("courier_list",watchedStudentService.getCouriersById(studentId));
        map.put("success",true);
        map.put("exc","");
        return map;

    }
    @ResponseBody
    @GetMapping("/stu/msg_new")
    public Map<String,Object> getReportMessages(@RequestParam("id")int id){
        Map<String,Object> map = new HashMap<>();
        map.put("list",watchedStudentService.getReportMessage(id));
        map.put("total",watchedStudentService.getReportMessageTotalNum(id));
        map.put("success",true);
        map.put("exc","");
        return map;
    }
    @ResponseBody
    @GetMapping("/stu/paper")
    public Map<String,Object> getQAndAById(@RequestParam("id")int id){
        return watchedStudentService.getQAndAById(id);
    }
}
