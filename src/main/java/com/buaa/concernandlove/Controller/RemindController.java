package com.buaa.concernandlove.Controller;

import com.buaa.concernandlove.Service.ReportService;
import com.buaa.concernandlove.Service.UserService;
import com.buaa.concernandlove.Service.WatchedStudentService;
import com.buaa.concernandlove.pojo.ReportRecord;
import com.buaa.concernandlove.pojo.User;
import com.buaa.concernandlove.pojo.WatchedStudent;
import com.buaa.concernandlove.utils.SendMessageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
public class RemindController {
    @Autowired
    UserService userService;
    @Autowired
    ReportService reportService;
    @Autowired
    WatchedStudentService watchedStudentService;
    @PostMapping("/stu/remind")
    public Map<String,Object> remindReport(@RequestParam("id") int watchedStudentId){
        Map<String,Object> ret=new HashMap<>();
        Map<String,Object> watchedStudentMap=new HashMap<>();
        try {
            watchedStudentMap=watchedStudentService.getWatchedStudentById(watchedStudentId).get(0);
        }
        catch (Exception e){
            ret.put("success",false);
            ret.put("exc","无该同学的信息");
            return ret;
        }
        List<User> reminderList=userService.getReportersByWatchedStudentId(watchedStudentMap.get("stuId").toString());
        System.out.println(reminderList.size());
        String exc="";
        boolean success=true;
        for(User reminder:reminderList){
            if(reportService.getTodayReportRecord(reminder.getUserid(),watchedStudentId)==null){
                String phone=reminder.getUserPhoneNumber();
                String url="http://39.106.1.86";
                String desc=reminder.getUsername()+"同学，今日关心关爱的同学信息还未上报，请及时上报"+url;
                try {
                    int code = SendMessageUtil.send(phone, desc);
                    if (code < 0) {
                        exc+=reminder.getUsername()+":"+SendMessageUtil.getMessage(code)+" ";
                        success=false;
                    }
                    if (code > 0){
                        exc+=reminder.getUsername()+":"+"短信提醒成功";
                    }
                }
                catch (Exception e){
                    exc+=reminder.getUsername()+":"+"短信发送失败";
                    success=false;
                }
            }
        }
        ret.put("success",success);
        ret.put("exc",exc);
        return ret;
    }
}