package com.buaa.concernandlove.pojo;

import java.sql.Timestamp;

public class AnswerRecord {
    private int answerRecordId;//自己的主键
    private int reportRecordId;//引用填报记录的主键
    private int questionId;//问题的id
    private int answer;//用户的答案
    private Timestamp answerTime;//用户作答时间

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getAnswerRecordId() {
        return answerRecordId;
    }

    public void setAnswerRecordId(int answerRecordId) {
        this.answerRecordId = answerRecordId;
    }

    public int getReportRecordId() {
        return reportRecordId;
    }

    public void setReportRecordId(int reportRecordId) {
        this.reportRecordId = reportRecordId;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public Timestamp getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(Timestamp answerTime) {
        this.answerTime = answerTime;
    }
}
