package com.buaa.concernandlove.pojo;

import com.alibaba.fastjson.JSONArray;

import java.sql.Timestamp;

public class ReportRecord {//上报记录
    private int recordId;//唯一主键
    private int reporterId;//上报员的id
    private int studentId;//学生的id
    private String specialCondition;//特殊情况
    private Timestamp reportTime;//填报时间

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getReporterId() {
        return reporterId;
    }

    public void setReporterId(int reporterId) {
        this.reporterId = reporterId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getSpecialCondition() {
        return specialCondition;
    }

    public void setSpecialCondition(String specialCondition) {
        this.specialCondition = specialCondition;
    }

    public Timestamp getReportTime() {
        return reportTime;
    }

    public void setReportTime(Timestamp reportTime) {
        this.reportTime = reportTime;
    }
}
