package com.buaa.concernandlove.pojo;

import com.alibaba.fastjson.JSONArray;

import java.sql.Timestamp;

public class Question {
    private int questionId;//题目的唯一id
    private int creatorId;//
    private int type;//1选择2判断
    private String questionBody;//题目名称
    private int isMust;//是否必填
    private Timestamp bornTime;//创建时间
    private JSONArray choices;//选择题才有

    public Question() {
    }

    public Question(int creatorId, int type, String questionBody, int isMust, JSONArray choices) {
        this.creatorId = creatorId;
        this.type = type;
        this.questionBody = questionBody;
        this.isMust = isMust;
        this.choices = choices;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getQuestionBody() {
        return questionBody;
    }

    public void setQuestionBody(String questionBody) {
        this.questionBody = questionBody;
    }

    public int getIsMust() {
        return isMust;
    }

    public void setMust(int must) {
        isMust = must;
    }

    public Timestamp getBornTime() {
        return bornTime;
    }

    public void setBornTime(Timestamp bornTime) {
        this.bornTime = bornTime;
    }

    public JSONArray getChoices() {
        return choices;
    }

    public void setChoices(JSONArray choices) {
        this.choices = choices;
    }
}
