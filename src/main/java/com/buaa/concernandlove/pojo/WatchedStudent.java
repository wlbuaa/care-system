package com.buaa.concernandlove.pojo;

public class WatchedStudent {
    private int studentId;
    private String studentName;
    private String phoneNum;
    private String stuId;//学工号
    private String grade;//年级
    private String classNum;//班级
    private String studentAvatarUrl;//照片
    private int isStopped;//现在是否停止追踪

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    public String getClassNum() {
        return classNum;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }

    public String getStudentAvatarUrl() {
        return studentAvatarUrl;
    }

    public void setStudentAvatarUrl(String studentAvatarUrl) {
        this.studentAvatarUrl = studentAvatarUrl;
    }

    public int getisStopped() {
        return isStopped;
    }

    public void setisStopped(int isStopped) {
        this.isStopped = isStopped;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
