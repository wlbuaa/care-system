package com.buaa.concernandlove.pojo;

public class User {
    private int userid;
    private String username;
    private String userStuId;//学工号；用这个登录
    private String userAvatarUrl;//头像的链接
    private String userPassword;//密码,数据库中需要明文存储
    private String userPhoneNumber;//上报员的电话号
    private String userRole;//
    private String userPasswordRaw;//没加密的密码
    private Boolean is_ban;

    public User() {
    }

    public User(String username, String userStuId, String userPassword, String userPhoneNumber, String userRole, String userPasswordRaw, Boolean is_ban) {
        this.username = username;
        this.userStuId = userStuId;
        this.userPassword = userPassword;
        this.userPhoneNumber = userPhoneNumber;
        this.userRole = userRole;
        this.userPasswordRaw = userPasswordRaw;
        this.is_ban = is_ban;
    }

    public int getUserid() {
        return userid;
    }

    public String getUserPasswordRaw() {
        return userPasswordRaw;
    }

    public void setUserPasswordRaw(String userPasswordRaw) {
        this.userPasswordRaw = userPasswordRaw;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserStuId() {
        return userStuId;
    }

    public void setUserStuId(String userStuId) {
        this.userStuId = userStuId;
    }

    public String getUserAvatarUrl() {
        return userAvatarUrl;
    }

    public void setUserAvatarUrl(String userAvatarUrl) {
        this.userAvatarUrl = userAvatarUrl;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public Boolean getBan() {
        return is_ban;
    }

    public void setBan(Boolean ban) {
        is_ban = ban;
    }
}
