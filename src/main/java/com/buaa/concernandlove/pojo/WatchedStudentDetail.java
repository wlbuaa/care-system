package com.buaa.concernandlove.pojo;

import java.util.ArrayList;
import java.util.List;

public class WatchedStudentDetail {
    private int studentId;
    private String studentName;
    private String phoneNum;
    private String stuId;//学工号
    private String classNum;//班级
    private String studentAvatarUrl;//照片
    private String dormNum;//宿舍号
    private String sex;
    private String homeAddress;
    private String grade;//年级
    private String classMonitorName;//班长姓名
    private String classMonitorPhoneNum;//班长电话
    private String mentorName;//导师姓名
    private String mentorPhoneNum;//导师电话
    private String emergencyContactName;//紧急联系人
    private String emergencyContactPhoneNum;//紧急联系人电话
    private String counselorName;//辅导员姓名
    private String counselorPhoneNum;//辅导员电话
    private String concernLevel;//关心等级
    private String concernReason;//关心关爱原因
    private String specialCondition;//特殊情况
    private List<Couriers> couriersList;//上报情况

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    public String getClassNum() {
        return classNum;
    }

    public void setClassNum(String classNum) {
        this.classNum = classNum;
    }

    public String getStudentAvatarUrl() {
        return studentAvatarUrl;
    }

    public void setStudentAvatarUrl(String studentAvatarUrl) {
        this.studentAvatarUrl = studentAvatarUrl;
    }

    public String getDormNum() {
        return dormNum;
    }

    public void setDormNum(String dormNum) {
        this.dormNum = dormNum;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getClassMonitorName() {
        return classMonitorName;
    }

    public void setClassMonitorName(String classMonitorName) {
        this.classMonitorName = classMonitorName;
    }

    public String getClassMonitorPhoneNum() {
        return classMonitorPhoneNum;
    }

    public void setClassMonitorPhoneNum(String classMonitorPhoneNum) {
        this.classMonitorPhoneNum = classMonitorPhoneNum;
    }

    public String getMentorName() {
        return mentorName;
    }

    public void setMentorName(String mentorName) {
        this.mentorName = mentorName;
    }

    public String getMentorPhoneNum() {
        return mentorPhoneNum;
    }

    public void setMentorPhoneNum(String mentorPhoneNum) {
        this.mentorPhoneNum = mentorPhoneNum;
    }

    public String getEmergencyContactName() {
        return emergencyContactName;
    }

    public void setEmergencyContactName(String emergencyContactName) {
        this.emergencyContactName = emergencyContactName;
    }

    public String getEmergencyContactPhoneNum() {
        return emergencyContactPhoneNum;
    }

    public void setEmergencyContactPhoneNum(String emergencyContactPhoneNum) {
        this.emergencyContactPhoneNum = emergencyContactPhoneNum;
    }

    public String getCounselorName() {
        return counselorName;
    }

    public void setCounselorName(String counselorName) {
        this.counselorName = counselorName;
    }

    public String getCounselorPhoneNum() {
        return counselorPhoneNum;
    }

    public void setCounselorPhoneNum(String counselorPhoneNum) {
        this.counselorPhoneNum = counselorPhoneNum;
    }

    public String getConcernLevel() {
        return concernLevel;
    }

    public void setConcernLevel(String concernLevel) {
        this.concernLevel = concernLevel;
    }

    public String getConcernReason() {
        return concernReason;
    }

    public void setConcernReason(String concernReason) {
        this.concernReason = concernReason;
    }

    public String getSpecialCondition() {
        return specialCondition;
    }

    public void setSpecialCondition(String specialCondition) {
        this.specialCondition = specialCondition;
    }

    public List<Couriers> getCouriersList() {
        return couriersList;
    }

    public void setCouriersList(List<Couriers> couriersList) {
        this.couriersList = couriersList;
    }
}
