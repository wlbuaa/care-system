package com.buaa.concernandlove.pojo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


public class Couriers {
    private String name;//上报员姓名
    private int role;//上报员角色
    private long startTime;//开始上报时间
    private long endTime;//结束上报时间
    private String frequency;//上报频率 dwm+123
    private JSONArray examId;//题目id列表
    private String reporterId;//上报人学工号

    public Couriers(JSONObject jsonObject){
        name = jsonObject.getString("name");
        role = jsonObject.getIntValue("role");
        startTime = jsonObject.getLongValue("start");
        endTime = jsonObject.getLongValue("end");
        reporterId = jsonObject.getString("num");
        frequency = jsonObject.getString("frequency");
        examId = jsonObject.getJSONArray("exam");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public JSONArray getExamId() {
        return examId;
    }

    public void setExamId(JSONArray examIds) {
        this.examId = examIds;
    }

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }
}
