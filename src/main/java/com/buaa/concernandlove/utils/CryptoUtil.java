//package com.buaa.concernandlove.utils;
//
//import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
//
//import javax.crypto.*;
//import javax.crypto.spec.SecretKeySpec;
//import java.nio.charset.StandardCharsets;
//import java.security.InvalidKeyException;
//import java.security.NoSuchAlgorithmException;
//import java.security.SecureRandom;
//
//public class CryptoUtil {
//    public static String encrypt(String content, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
//        KeyGenerator kgen = KeyGenerator.getInstance("AES");
//        kgen.init(128, new SecureRandom(password.getBytes()));
//        SecretKey secretKey = kgen.generateKey();
//        byte[] enCodeFormat = secretKey.getEncoded();
//        SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
//        Cipher cipher = Cipher.getInstance("AES");
//        byte[] byteContent = content.getBytes(StandardCharsets.UTF_8);
//        cipher.init(Cipher.ENCRYPT_MODE, key);
//        byte[] result = cipher.doFinal(byteContent);
//        return Base64.encode(result);
//    }
//
//    public static String decrypt(String ciphertext, String password) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
//        KeyGenerator kgen = KeyGenerator.getInstance("AES");
//        kgen.init(128, new SecureRandom(password.getBytes()));
//        SecretKey secretKey = kgen.generateKey();
//        byte[] enCodeFormat = secretKey.getEncoded();
//        SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
//        Cipher cipher = Cipher.getInstance("AES");
//        cipher.init(Cipher.DECRYPT_MODE, key);
//        byte[] byte_cipher = Base64.decode(ciphertext);
//        byte[] result = cipher.doFinal(byte_cipher);
//        return new String(result);
//    }
//}
