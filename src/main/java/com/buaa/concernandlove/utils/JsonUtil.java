package com.buaa.concernandlove.utils;

import com.alibaba.fastjson.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class JsonUtil {
    public static List<String> jsonToList(String jsonStr) {
        List<String> list = new ArrayList<>();
        JSONArray jsonArray = JSONArray.parseArray(jsonStr);
        for (Object o : jsonArray) {
            list.add(o.toString());
        }
        return list;
    }

    public static List<String> ObjectToList(Object obj) {
        List<String> res = new ArrayList<>();
        if(obj instanceof List<?>) {
            for(Object o : (List<?>) obj) {
                res.add((String) o);
            }
            return res;
        }
        else return null;
    }
}
