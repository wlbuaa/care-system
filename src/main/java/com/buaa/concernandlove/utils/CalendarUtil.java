package com.buaa.concernandlove.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CalendarUtil {
    public static Calendar getFirstDay(int weekNum){
        Calendar calendar = Calendar.getInstance();
        calendar.set(2021,Calendar.SEPTEMBER,6,0,0,0); // 当前学期的第一周第一天
        calendar.add(Calendar.DATE,(weekNum-1)*7);
        return calendar;
    }

    public static Date findNextExecTime(List<String> timeList) {
        //以下为找到下次执行任务的时间
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date now = null;
        try {
            now = dateFormat.parse(dateFormat.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date next = null, earlistDate = null;
        System.out.println("timeList: " + timeList.toString());
        for(String time : timeList) {
            try {
                Date date = dateFormat.parse(time);
                //找到在now之后的最早的date
                if(date.after(now)) {
                    if(next == null) {
                        next = date;
                    }
                    else if(date.before(next)){
                        next = date;
                    }
                }

                //获取最早时间
                if(earlistDate == null) {
                    earlistDate = date;
                }
                else if(earlistDate.after(date)){
                    earlistDate = date;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Date nextExecTime = null;
        if(next != null) {
            nextExecTime = new Date();
            nextExecTime.setHours(next.getHours());
            nextExecTime.setMinutes(next.getMinutes());
            nextExecTime.setSeconds(next.getSeconds());
        }
        else {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            nextExecTime = calendar.getTime();

            nextExecTime.setHours(earlistDate.getHours());
            nextExecTime.setMinutes(earlistDate.getMinutes());
            nextExecTime.setSeconds(earlistDate.getSeconds());
        }
        System.out.println("Next Execution Time is "+nextExecTime.toString());
        return nextExecTime;
    }

    public static Timestamp getNowDayTimestamp(Timestamp timestamp){
        Calendar inTime = Calendar.getInstance();
        Calendar outTime = Calendar.getInstance();
        Date date = new Date(timestamp.getTime());
        inTime.setTime(date);
        outTime.set(Calendar.HOUR_OF_DAY,inTime.get(Calendar.HOUR_OF_DAY));
        outTime.set(Calendar.MINUTE,inTime.get(Calendar.MINUTE));
        outTime.set(Calendar.SECOND,0);
        return new Timestamp(outTime.getTimeInMillis());
    }

    public static Timestamp getNowWeekTimestamp(Timestamp timestamp){
        Calendar inTime = Calendar.getInstance();
        Calendar outTime = Calendar.getInstance();
        Date date = new Date(timestamp.getTime());
        inTime.setTime(date);
        outTime.set(Calendar.DAY_OF_WEEK,inTime.get(Calendar.DAY_OF_WEEK));
        outTime.set(Calendar.HOUR_OF_DAY,inTime.get(Calendar.HOUR_OF_DAY));
        outTime.set(Calendar.MINUTE,inTime.get(Calendar.MINUTE));
        outTime.set(Calendar.SECOND,0);
        return new Timestamp(outTime.getTimeInMillis());
    }

    public static Timestamp getNowMonthTimestamp(Timestamp timestamp){
        Calendar inTime = Calendar.getInstance();
        Calendar outTime = Calendar.getInstance();
        Date date = new Date(timestamp.getTime());
        inTime.setTime(date);
        outTime.set(Calendar.MONTH,inTime.get(Calendar.MONTH));
        outTime.set(Calendar.HOUR_OF_DAY,inTime.get(Calendar.HOUR_OF_DAY));
        outTime.set(Calendar.MINUTE,inTime.get(Calendar.MINUTE));
        outTime.set(Calendar.SECOND,0);
        return new Timestamp(outTime.getTimeInMillis());
    }
}
